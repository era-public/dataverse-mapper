# geomapper
Based on [geotiff.io](https://github.com/GeoTIFF/geotiff.io/), this application is intended as external tool for [Dataverse](https://github.com/IQSS/dataverse), in particular for [GRO.data](https://data.goettingen-research-online.de/), the Dataverse repository of the University of Göttingen.

Within GRO.Data, a button is provided for registered file types (zipped shapefiles, (Geo-)Tiff, GeoJSON, KML) that links to the application. Alternatively, you can search GRO.data from within the application via the provided interface.

## Background
GeoTiffs are georeferenced `.tif/.tiff` files that are commonly used to store geographic raster data such as digital elevation models or satellite images.  [Dataverse](https://github.com/IQSS/dataverse) currently does not support visualization of uploaded GeoTiff images in a Web map. Even [ArcGIS Online](https://support.esri.com/en/technical-article/000016852) does not support direct visualization on a map, only after publication as a tile service.

Moreover, the application supports visualization of other common geo data files: shapefiles, GeoJSON, KML and CSV files with latitude/longitude coordinates.

# Current Features

- Load and view GeoTiff files, (zipped) Shapefiles, GeoJSON files, KML and CSV files (with geographic coordinates) directly in the browser on a map
- Apply different color scales to a single-band GeoTiff and view min/max values
- Select band order and view false-color composites for multi-band GeoTiffs
- Visualize vector data using the options "Single color", "Categorized" or "Graduated", which includes Natural Breaks, Equal Interval etc.
- View a legend for vector data when applying color options 
- Zoom to, delete, or rearrange the order of your layers using the layer control 
- Search GRO.Data for files, datasets and/or dataverses using the search tool within the application and view supported geo data file types directly on the map
- List all files of a dataset and select which of the supported files to add to the map
- Filter search results by 'Geospatial' tags or by country (if available in the metadata)
- Search GRO.data by drawing a rectangular search area on the map and retrieve datasets with associated bounding boxes that intersect with this area
- Add a geographich bounding box to your dataset or modify the existing one (all changes will first result in a new Draft version of your dataset which can then be published)
- Add an API token from GRO.Data to access non-public files in the search tool or edit your bounding box metadata


# Documentation
A full documentation can be found  [here](https://gitlab.gwdg.de/era-public/dataverse-mapper/-/wikis/home).

# Configuration

- The application name and Dataverse URL can be adjusted in the `.env` file
- In `src/constants/tools-loaded.json`, you can specify which tools to load (currently, only three tools are available)

# Get started

For viewing and testing the application without Docker and therefore without map-based search:

 - `npm install`
 - `npm start`

As Docker setup including containers for Web application, harvester API (for harvesting bounding box metadata from GRO.Data) and Solr (for indexing bounding box metadata):

- `sh initialize.sh`

For building the Docker images locally, comment out the image names in `docker-compose.yaml` and refer to the two local Docker files.


