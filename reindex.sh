#!/bin/bash
# Harvests and indexes bounding boxes. Requires API and Solr to be running.
now=$(date +"%T")
echo "Harvesting and indexing bounding boxes at $now!"
curl "localhost:5000/harvest"

exit 0
