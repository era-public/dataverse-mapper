'use strict';
import express from 'express';
import fetch from 'node-fetch';
import cors from 'cors';

const app = express();
const dataverseURL = "https://data.goettingen-research-online.de";
const searchURL = dataverseURL + '/api/v1/search?q=westLongitude:*&type=dataset&per_page=50';
const port = 5000; //as in Dockerfile

app.use(cors());
async function collectDatasets() {
    let dataset_ids = [];
    const searchResponse = await fetch(searchURL);
    const jsonData = await searchResponse.json();
    if (jsonData["status"] === 'OK'){
        const data = jsonData["data"];
        let totalCount = data["total_count"];
        let numberOfRuns = Math.ceil(totalCount/50);
        for (let i = 0; i< numberOfRuns; i++){
            await new Promise(resolve => setTimeout(resolve, 250));
            const offsetResponse = await fetch(searchURL+'&start='+String(i));
            const jsonData = await offsetResponse.json();
            const items = jsonData['data']['items'];
            items.forEach(item => {
                if (item['versionState'] !== 'DRAFT')
                    dataset_ids.push(item['global_id']);
            });
        }
    }
    return dataset_ids;
}
async function indexBoundingBoxes(collectedIDs) {
    let documentArray = [];
    for (const datasetID of collectedIDs) {
        await new Promise(resolve => setTimeout(resolve, 250));
        let metadataURL = dataverseURL + '/api/datasets/:persistentId/versions/:latest-published?persistentId=' + datasetID;
        const datasetResponse = await fetch(metadataURL);
        const jsonResult = await datasetResponse.json();
        const data = jsonResult["data"];
        const metaData = data["metadataBlocks"];
        let title = '';
        if (metaData && metaData["geospatial"]) {
            for (const field of metaData["citation"]["fields"]) {
                if (field["typeName"] === "title")
                    title = field["value"];
            }
            console.log(`Dataset ${datasetID}: ${title}`);
            if (metaData["geospatial"]["fields"]) {
                const geospatial = metaData["geospatial"]["fields"];
                for (const field of geospatial) {
                    let fieldType = field["typeName"];
                    let value = field["value"];
                    for (const box of value) { //single bounding box
                        if (fieldType === "geographicBoundingBox") {
                            let west = -180;
                            let east = 180;
                            let north = 90;
                            let south = -90;
                            for (const [key, value] of Object.entries(box)) {
                                if (key === "westLongitude") west = value["value"];
                                else if (key === "eastLongitude") east = value["value"];
                                else if (key === "northLongitude") north = value["value"];
                                else if (key === "southLongitude") south = value["value"];
                            }
                            let envelope = `ENVELOPE(${west},${east},${north},${south})`;
                            console.log("Bounding Box: ", envelope);
                            let document = {}; // document to index
                            document["id"] = datasetID;
                            document["solr_geom"] = envelope;
                            documentArray.push(document);
                        }
                    }
                }
            }
        }
    }
    if (documentArray.length === 0) return {'message': 'No new datasets found! Index remains the same.'};
    // if the old index contains IDs not present in the current index, delete the dataset
    try {
        const idResponse = await fetch('http://geomapper-solr:8983/solr/geomapper/select?q.op=OR&q=*:*'); // get current index
        const jsonResponse = await idResponse.json();
        if (jsonResponse.response && jsonResponse.response.numFound > 0) {
            jsonResponse.response.docs.forEach(existingDocument => {
                if (!collectedIDs.includes(existingDocument.id)){
                    console.log(`Dataset with ID ${existingDocument.id} could no longer be found! Deleting from index...`); //'*:*'
                    new Promise( (resolve) => {
                        resolve(deleteFromIndex(`id:"${existingDocument.id}"`));
                    }).then(responseMessage => {
                        console.log(responseMessage);
                    });
                }
            });
        }
    } catch (e) {
        console.log("An error occurred when checking for existing index. Continue with indexing...");
    }
    // due to the unique ID, only new datasets will be added to the index
    let response = {'error': 'Indexing failed!'};
    try {
        const indexResponse = await fetch('http://geomapper-solr:8983/solr/geomapper/update?commit=true',
            {method: 'POST', body: JSON.stringify(documentArray), headers: {'Content-Type': 'application/json'}});
        const indexResult = await indexResponse.json();
        if (indexResult["responseHeader"]["status"] === 0){
            response = {'message': 'Indexing successfull!'};
        }
    } catch (e) {
        response = {'error': e};
    }
    return response;
}

async function deleteFromIndex(queryString){
    let response = {'error': 'Deleting index failed!'};
    try {
        const indexResponse = await fetch('http://geomapper-solr:8983/solr/geomapper/update?commit=true',
            {method: 'POST', body: JSON.stringify({'delete': {'query': queryString}}), headers: {'Content-Type': 'application/json'}});
        const indexResult = await indexResponse.json();
        if (indexResult["responseHeader"]["status"] === 0) {
            console.log(indexResult);
            response = {'message': 'Index deleted!'};
        }
    } catch (e) {
        response = {'error': e};
    }
    return response;
}

/**
 * Searches GeoMapper's Solr index for datasets with bounding boxes that are intersecting the query bounding box
 * */

app.get('/geoquery', async (req, res) => {
    let envelope = req.query.envelope;
    if (!envelope) res.send({error: 'Please provide an envelope: ?envelope=(west, east, north, south)'});
    const queryURL = 'http://geomapper-solr:8983/solr/geomapper/select?q.op=OR&q=%7B!field%20f%3Dsolr_geom%7DIntersects('+envelope+')';
    const bboxResponse = await fetch(queryURL);
    const jsonResponse = await bboxResponse.json();
    let status = jsonResponse['responseHeader'].status;
    if (status === 0) res.send(jsonResponse);
    else res.send({error: jsonResponse['error']['msg']});
});

/**
 * Searches GRO.Data for datasets containing bounding boxes, converts bounding box values to Solr-compatible format and updates GeoMapper's Solr index
 * */
app.get('/harvest', (req, res) => {
    new Promise( (resolve) => {
        resolve(collectDatasets());
    }).then(datasets => {
         new Promise( (resolve) => {
             resolve(indexBoundingBoxes(datasets));
        }).then(response => {
             res.json(response);
         });
    });
});

app.get('/', (req, res) => {
    res.send({message: 'Please use one of the endpoints /geoquery or /harvest.'});
});

app.listen(port, () => {
    console.log(`GeoMapper Search API listening on port ${port}!`);
});

