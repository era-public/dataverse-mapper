#!/bin/bash

solr_url=http://localhost:8983/solr
solr_core=geomapper
api_port=5000
solr_schema_url=${solr_url}/${solr_core}/schema

schema_fields="@/data/solr-fields.json"
echo "Creating fields..."
curl -X POST -H 'Content-type:application/json' ${solr_schema_url} -d ${schema_fields}
curl "${solr_url}/admin/cores?action=RELOAD&core=${solr_core}"
echo "Harvesting and indexing..."
curl "geomapper-api:${api_port}/harvest"
