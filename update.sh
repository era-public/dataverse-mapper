#!/bin/bash
# Pulls and updates new API and app images but leaves the Solr index running.
docker-compose pull app && \
docker-compose pull api && \
docker-compose up -d \

exit 0
