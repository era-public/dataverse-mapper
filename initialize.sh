#!/bin/bash
# (Re-)builds everything, including Solr.
docker-compose down -v --remove-orphans && \
docker-compose pull app && \
docker-compose pull api && \
docker-compose up -d --build && \
sleep 5 && \
docker-compose exec -T solr /data/init.solr.sh

exit 0
