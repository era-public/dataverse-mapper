module.exports = {
    root: true,
    env: {
        node: true,
        es6: true,
        browser: true,
        jest: true,
    },
    parser: '@babel/eslint-parser',
    extends: [
        'eslint:recommended',
        'plugin:react/recommended',
    ],
    parserOptions: {
        requireConfigFile: false,
        'ecmaVersion': 2020,
        'sourceType': 'module',
        "babelOptions": {
            "presets": ["@babel/preset-react"],
        },
    },
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-continue': 0,
        'no-floating-decimal': 0,
        'no-tabs': 0,
        'max-len': ['error', { code: 180 }],
        'comma-dangle': ['error', {
            arrays: 'always-multiline',
            objects: 'always-multiline',
            imports: 'always-multiline',
            exports: 'always-multiline',
            functions: 'always-multiline',
        }],
        'semi-style': ['error', 'last'],
        'semi': ['error', 'always', { omitLastInOneLineBlock: true }],
        'no-return-assign': 'off',
        'dot-notation': 'off',
        "react/prop-types": "off",
    },
    overrides: [
        {
            files: [
                '**/__tests__/*.{j,t}s?(x)',
                '**/tests/unit/**/*.spec.{j,t}s?(x)',
            ],
            env: {
                jest: true,
            },
        },
    ],
};
