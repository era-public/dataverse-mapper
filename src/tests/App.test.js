import React from 'react';
import ReactDOM from 'react-dom';
import App from '../components/app';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from "react-redux";
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
const middlewares = [thunk];
import { unmountComponentAtNode } from "react-dom";
import {loadTools} from "../actions/tool-list-actions";
import {addAPIKey} from "../actions/api-actions";
import {API_KEY_ADDED} from "../constants/actions";
import UrlService from "../services/UrlService";

const mockStore = configureMockStore(middlewares);
const storeContainsObject = (actions, type, payload) => {
    expect(actions).toEqual(
        expect.arrayContaining([
            expect.objectContaining({
                type: type,
                payload: payload,
            }),
        ]),
    );
};

describe('Basic App test', () => {
    let container = null;
    let store = null;
    beforeEach(() => {
        container = document.createElement("root");
        document.body.appendChild(container);
        store = mockStore({toolList: []});
        store.dispatch(loadTools());
    });

    afterEach(() => {
        unmountComponentAtNode(container);
        container.remove();
        container = null;
    });
    it('renders without crashing', () => {
        ReactDOM.render(
            <Provider store={store}>
                <BrowserRouter basename="/">
                    <App/>
                </BrowserRouter>
            </Provider>,
            container,
        );
    });

    it('is able to read and store API key from URL', () => {
        const mockedReplace = jest.fn();
        const originalWindow = { ...window };
        const windowSpy = jest.spyOn(global, "window", "get");
        windowSpy.mockImplementation(() => ({
            ...originalWindow,
            location: {
                ...originalWindow.location,
                search: "?fileID=myFileId&key=myApiKey&siteUrl=https://my-dataverse.example.com",
                replace: mockedReplace,
            },
        }));
        const apiKey = UrlService.get('key');
        expect(apiKey).toEqual('myApiKey');
        store.dispatch(addAPIKey(apiKey));
        return storeContainsObject(store.getActions(), API_KEY_ADDED, 'myApiKey');
    });

});
