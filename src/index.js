import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import App from './components/app';
import UrlService from './services/UrlService';
import reportWebVitals from './reportWebVitals';
import buildStore from './build-store';
import * as OfflinePluginRuntime from 'offline-plugin/runtime';
import {Provider} from 'react-redux';
import {loadTools} from './actions/tool-list-actions';
import {addAPIKey} from './actions/api-actions';
import {setRequestedURL} from "./actions/file-actions";
import { MuiThemeProvider, createTheme } from '@material-ui/core/styles';
import {startLoading, stopLoading} from "./actions/loading-actions";
import FileService from "./services/FileService";
import {showAlert} from "./actions/alert-actions";
import {addRaster, addVectorData} from "./actions/layer-actions";
import './assets/scss/style.scss';
import Map from './map/Map';

OfflinePluginRuntime.install();
const geomapperTheme = createTheme({
    palette: {
        primary: {
            main: 'rgba(0, 101, 151, 1)',
        },
        secondary: {
            main: 'rgba(49, 185, 141, 1)',
        },
    },
});
const store = buildStore();
store.dispatch(loadTools());
window.store = store; // for Map.js

const keyInURL = UrlService.get('key');
if (!keyInURL && process.env.REACT_APP_API_KEY){
    store.dispatch(addAPIKey(process.env.REACT_APP_API_KEY));
} else if (keyInURL) {
    store.dispatch(addAPIKey(keyInURL));
    UrlService.removeAndReload('key');
}
const dataverseURL = UrlService.get('siteUrl');
const url = UrlService.get('url');
if (url) store.dispatch(setRequestedURL(url));
else if (dataverseURL) {
    const fileID = UrlService.get('fileId');
    if (fileID) {
        let fileUrl = dataverseURL + '/api/v1/access/datafile/' + fileID + '?gbrecs=true';
        if (keyInURL) fileUrl+='&key='+keyInURL;
        store.dispatch(startLoading('Loading File'));
        FileService.getDataverseFileType(fileUrl, fileID, store.dispatch).then(fileMetadata => {
            FileService.processFileType(fileMetadata, addVectorData, addRaster, store.dispatch).then(result => {
                store.dispatch(stopLoading());
                if (!result || result !== 'SUCCESS') {
                    store.dispatch(showAlert(`${process.env.REACT_APP_TITLE} was unable to load the file.`));
                }
            });
        }).catch((e) => {
            store.dispatch(stopLoading());
            store.dispatch(showAlert(e));
        });
    }
}

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter basename='/'>
            <MuiThemeProvider theme={geomapperTheme}>
                <App/>
            </MuiThemeProvider>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'),
);

Map.initialize();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
