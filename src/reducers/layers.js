import {MAP_LAYER_ADDED, MAP_LAYER_REMOVED} from '../constants/actions';

const layers = (state = [], action) => {

    switch (action.type) {
        case MAP_LAYER_ADDED:
            return [...state, action.payload];
        case MAP_LAYER_REMOVED:
            return state.filter((item) => item.id !== action.payload);
        default:
            return state;
    }
};

export default layers;
