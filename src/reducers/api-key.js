import {API_KEY_ADDED, API_KEY_REMOVED} from '../constants/actions';

const apiKey = (state = null, action) => {
    switch (action.type) {
        case API_KEY_ADDED:
            return action.payload;
        case API_KEY_REMOVED:
            return null;
        default:
            return state;
    }
};

export default apiKey;
