import {RESULTS_CLEAR, RESULTS_ITEM_UPDATE, RESULTS_SET, RESULTS_UPDATE} from '../constants/actions';

const results = (state = null, action) => {
    switch (action.type) {
        case RESULTS_SET:
            return action.results;
        case RESULTS_UPDATE:
            return {
                ...state,
                [action.obj.key]: action.obj.value,
            };
        case RESULTS_ITEM_UPDATE: {
           return {
                ...state,
                items: [...state.items].map(prop => {
                    if (prop.key === action.obj.key) {
                        return {
                            ...prop,
                            [action.obj.property]: action.obj.value,
                        };
                    }
                    return {...prop};
                }),
            };
        }
        case RESULTS_CLEAR:
            return null;
        default:
            return state;
    }
};

export default results;
