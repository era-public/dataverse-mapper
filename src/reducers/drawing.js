import Map from '../map/Map';
import {MAP_DRAW_START, MAP_DRAW_STOP} from "../constants/actions";

const drawing = (state = null, action) => {
    switch (action.type) {
        case MAP_DRAW_START:
            if (action.format.mode === 'drawing') {
                Map.startDrawRectangle();
            } else if (action.format.mode === 'editing')
                Map.startEditRectangle(action.format.currentBoxes);
            return action.format;
        case MAP_DRAW_STOP:
            Map.stopDrawRectangle(true);
            return null;
        default:
            return state;
    }
};

export default drawing;
