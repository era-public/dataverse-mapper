import {SEARCH_ENVELOPE_ADDED, SEARCH_ENVELOPE_REMOVE} from '../constants/actions';

const searchEnvelope = (state = null, action) => {
    switch (action.type) {
        case SEARCH_ENVELOPE_ADDED:
            return action.payload;
        case SEARCH_ENVELOPE_REMOVE:
            return null;
        default:
            return state;
    }
};

export default searchEnvelope;
