import {POPOVER_HIDE, POPOVER_SHOW} from '../constants/actions';

const popover = (state = false, action) => {
    switch (action.type) {
        case POPOVER_HIDE:
            return false;
        case POPOVER_SHOW:
            return true;
        default:
            return state;
    }
};

export default popover;
