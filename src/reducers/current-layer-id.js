import {MAP_LAYER_SELECTED, MAP_LAYER_UNSELECTED} from '../constants/actions';

const currentLayerId = (state = null, action) => {
    switch (action.type) {
        case MAP_LAYER_SELECTED:
            return action.payload;
        case MAP_LAYER_UNSELECTED:
            return null;
        default:
            return state;
    }
};

export default currentLayerId;
