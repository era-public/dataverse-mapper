import {BOUNDING_BOX_ADD, BOUNDING_BOX_REMOVE} from "../constants/actions";

const bbox = (state = null, action) => {
    switch (action.type) {
        case BOUNDING_BOX_ADD:
           return action.geometry;
        case BOUNDING_BOX_REMOVE:
            return null;
        default:
            return state;
    }
};

export default bbox;
