import {TOOL_LIST_LOAD} from '../constants/actions';

const toolList = (state = [], action) => {
    switch (action.type) {
        case TOOL_LIST_LOAD:
            return action.payload;
        default:
            return state;
    }
};

export default toolList;
