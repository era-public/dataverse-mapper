import {combineReducers} from 'redux';
import activeTool from './active-tool';
import alert from './alert';
import layout from './layout';
import loading from './loading';
import menuFocus from './menu-focus';
import currentLayerId from './current-layer-id';
import layers from './layers';
import results from './results';
import toolList from './tool-list';
import apiKey from './api-key';
import popover from './popover';
import requestedFile from "./requested-file";
import drawing from "./drawing";
import bbox from "./bbox";
import searchEnvelope from "./search-envelope";

const rootReducer = combineReducers({
    activeTool,
    alert,
    layout,
    loading,
    menuFocus,
    currentLayerId,
    layers,
    results,
    toolList,
    apiKey,
    popover,
    requestedFile,
    drawing,
    bbox,
    searchEnvelope,
});

export default rootReducer;
