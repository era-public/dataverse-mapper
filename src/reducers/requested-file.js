import {EXTERNAL_URL_ADDED, EXTERNAL_URL_REMOVED} from '../constants/actions';

const requestedFile = (state = null, action) => {
    switch (action.type) {
        case EXTERNAL_URL_ADDED:
            return action.payload;
        case EXTERNAL_URL_REMOVED:
            return null;
        default:
            return state;
    }
};

export default requestedFile;
