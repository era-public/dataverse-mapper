import {useDispatch, useSelector} from "react-redux";
import InfoPopoverComponent from "./popover.component";
import {hidePopover, showPopover} from "../../actions/popover-actions";

const InfoPopoverContainer = ({inputFocused, className, content}) => {
    const popover = useSelector((state) => state.popover);
    const dispatch = useDispatch();
    const hide = () => {
        return dispatch(hidePopover());
    };
    const show = () => {
        return dispatch(showPopover());
    };
    return InfoPopoverComponent({inputFocused, popover, hide, show, className, content});
};

export default InfoPopoverContainer;
