import React, {useState} from "react";
import Popover from '@mui/material/Popover';

const InfoPopoverComponent = ({inputFocused, popover, hide, show, className, content}) => {

    const [anchorEl, setAnchorEl] = useState(null);
    const handleClick = (event) => {
        if (!popover) {
            show();
            setAnchorEl(event.currentTarget);
        } else if (!inputFocused) {
            hide();
            setAnchorEl(null);
        }
    };
    const open = Boolean(anchorEl);
    const id = open ? 'popover_'+className : undefined;

    return (
            <div className={className} onClick={handleClick}>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={hide}
                anchorOrigin={{
                    vertical: 'center',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'center',
                    horizontal: 'left',
                }}
            >
                {content}
            </Popover>
        </div>
    );
};

export default InfoPopoverComponent;
