import React from 'react';
import ToolButton from '../tool-elements/tool-button';

const MenuComponent = ({toolList, layout}) => (
    <div id='menu' className={layout === 'hide-menu' ? 'menu-hidden' : 'menu'}>
        <header id='menu-header'>
            <div className={'menu-header-icon'}/>
            <menu-header-title>{process.env.REACT_APP_TITLE}</menu-header-title><br/>
            <menu-header-text>Visualize geospatial
                files from <a target="_blank" rel="noreferrer" href={process.env.REACT_APP_DATAVERSE_URL}>
                    {process.env.REACT_APP_DATAVERSE}</a> research repository,<br/> local desktop or remote links.<br/>
                <br/><br/>
            </menu-header-text>
        </header>
        <section id='content'>
            <div id='tool-button-container'>
                {
                    toolList.map(({name, iconClass, path, tooltip}) => {
                        return <ToolButton
                            iconClass={iconClass}
                            key={name}
                            name={name}
                            path={path}
                            tooltip={tooltip}
                        />;
                    })
                }
            </div>

        </section>
    </div>
);

export default MenuComponent;
