import MenuComponent from './menu.component';
import {useSelector} from 'react-redux';
const MenuContainer = () => {

    const toolList = useSelector((state) => state.toolList);
    const layout = useSelector((state) => state.layout);
    // const menuFocus = useSelector((state) => state.menuFocus);
    return MenuComponent({
         toolList, layout,
    });

};

export default MenuContainer;
