import React from 'react';
import Select from 'react-select';
import {customSelectStyle} from "../tool-elements/tool-content/tool-content.component";

const LayerSelectionComponent = ({selectedLayer, selectLayer, options}) => (

    <div>
        <Select styles={customSelectStyle}
            value={options && options.filter(option => {
                return option.value === selectedLayer;
            })}
            placeholder="Select data layer..."
            isSearchable={false}
            onChange={selectLayer}
            options={options}
        />
    </div>
);

export default LayerSelectionComponent;
