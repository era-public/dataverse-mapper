import ColorizeComponent from './colorize.component';
import {updateSelectedLayer} from '../../../actions/layer-actions';
import { useSelector, useDispatch } from 'react-redux';
import Map from '../../../map/Map';
import RasterService from "../../../services/RasterService";
import chroma from 'chroma-js';
import {
    CATEGORY,
    GRADUATED, MODE_GEOM,
    MODE_INTERVAL, MODE_JENKS,
    MODE_QUANTILE, MODE_STD,
    RASTER,
    SINGLE_COLOR,
    VECTOR,
} from '../../../constants/variables';
import randomColor from 'randomcolor';
import geostats from 'geostats';
import {useEffect, useState} from "react";

const generateColorBar = (colorScaleType) => {

    if (colorScaleType === 'Default') return null;
    let scaleType = getChromaScaleType(colorScaleType);
    let scale = chroma.scale(scaleType).domain([0, 1]);
    if (colorScaleType === 'Spectral Reverse' || colorScaleType === 'Green-Red') {
        scale = chroma.scale(scaleType).domain([1, 0]);
    }
    const colors = [];
    for (let i = 0; i < 100; i++) {
        colors.push({
            'key': i,
            'value': scale(i/100).hex(),
        });
    }
    return colors;
};

const getColorBars = (type) => {
    let colorBars = [];
    if (type === RASTER) colorBars.push({'value': 'Default', 'label': 'Default Color Style'});
    else if (type === CATEGORY) colorBars.push({'value': 'Default', 'label': 'Random Colors'});
    let defaultOptions = ['Viridis', 'YlGnBu', 'Spectral', 'Spectral Reverse', 'Red-Green', 'Green-Red', 'Greys', 'Greens','Reds', 'Blues'];
    defaultOptions.forEach((value) => {
        colorBars.push({
            'value': value,
            'label': value,
        });
    });
    return colorBars;
};

const colorizeRaster = (layers, id, colorScaleType, opacity, rasterMin, rasterMax, redBand, greenBand, blueBand, removeNoData) => {
    const raster = getLayerById(layers, id);
    const isSingleBand = raster.options.georaster.numberOfRasters < 3;
    let pixelValuesToColorFn;
    if (isSingleBand){
        if (colorScaleType === 'Default') {
            if (raster.palette) {
                pixelValuesToColorFn =
                    RasterService.applyColorPalette(raster, removeNoData);
            } else pixelValuesToColorFn = null; //reset to default
        } else {
            let scaleType = getChromaScaleType(colorScaleType);
            let scale = chroma.scale(scaleType).domain([0, 1]);
            if (colorScaleType === 'Spectral Reverse' || colorScaleType === 'Green-Red') {
                scale = chroma.scale(scaleType).domain([1, 0]);
            }
            pixelValuesToColorFn = pixelValues => {
                const pixelValue = pixelValues[0]; // there's just one band in this raster
                if (removeNoData && (pixelValue === raster.noDataValue || pixelValue === 0)) return 0;
                const normalized = (pixelValue - rasterMin) / (rasterMax - rasterMin);
                return scale(normalized).hex();
            };
        }
    }  else {
        pixelValuesToColorFn =
            RasterService.createMultiBandColors(raster, redBand-1, greenBand-1, blueBand-1, removeNoData);
    }
    raster.options.opacity = opacity;
    raster.options.removeNoData = removeNoData;
    raster.options.colorScaleType = colorScaleType;
    raster.options.pixelValuesToColorFn = pixelValuesToColorFn;

    Map.updateRasterLayout(raster.id);

};

const getRasterProperties = rasterLayer => {
    if (!rasterLayer.options.colorScaleType) rasterLayer.options.colorScaleType = 'Default';
    if (!rasterLayer.options.opacity) rasterLayer.options.opacity = 0.8;
    if (!rasterLayer.options.removeNoData) rasterLayer.options.removeNoData = false;
    const numberOfBands = rasterLayer.options.georaster.numberOfRasters;
    const noDataValue = String(rasterLayer.options.georaster.noDataValue);
    const colorScaleType = rasterLayer.options.colorScaleType;
    const opacity = rasterLayer.options.opacity;
    const removeNoData = rasterLayer.options.removeNoData;
    const min = rasterLayer.options.georaster.mins ? parseFloat(rasterLayer.options.georaster.mins[0]) : 0;
    const max = rasterLayer.options.georaster.maxs ? parseFloat(rasterLayer.options.georaster.maxs[0]) : 100;
    return {'min': min, 'max': max, 'noDataValue': noDataValue, 'numberOfBands': numberOfBands, 'colorScaleType': colorScaleType, 'opacity': opacity, 'removeNoData': removeNoData};
};

const getLayerDropDownList = (layers) => {
    let layerListDropDown = [{value: "noLayer", label: "--No layer--"}];
    if (layers && layers.length > 0){
        layerListDropDown = [];
        layers.forEach(layer => layerListDropDown.push({id: layer.id, value: layer.id, label: layer.name}));
    }
    return layerListDropDown;
};

const getClassificationOptions = () => {
    return [
        {value: MODE_JENKS, label: 'Natural Breaks (Jenks)'},
        {value: MODE_INTERVAL, label: 'Equal Interval'},
        {value: MODE_QUANTILE, label: 'Equal Count (Quantile)'},
        {value: MODE_GEOM, label: 'Geometric Interval'},
        {value: MODE_STD, label: 'Standard Deviation'},
    ];
};

const getColorTypeOptions = () => {
    return [
        {value: SINGLE_COLOR, label: "Single Color"},
        {value: CATEGORY, label: "Categorized"},
        {value: GRADUATED, label: "Graduated"},
    ];
};


const getLayerById = (layers, id) => {

    const layerWithID = layers.filter(layer => String(layer.id) === String(id));
    if (layerWithID.length < 1) {
        return null;
    }
    return layerWithID[0];
};

const getListOfBands = (numberOfBands) => {
    const bands = [];
    for (let i = 1; i <= numberOfBands; i++){
        bands.push({'key': i, 'value': i, 'label': 'Band ' + i});
    }
    return bands;
};

const getChromaScaleType = (colorScaleType) => {
    if (colorScaleType === 'Spectral Reverse') return 'Spectral';
    else if (colorScaleType === 'Red-Green' || colorScaleType === 'Green-Red') return 'RdYlGn';
    else return colorScaleType;
};

const toNumericArray = (array) => {
    const numericArray = [];
    array.forEach((value) => {
        if (!value || value === '') return;
        if (isNaN(value)) value = parseFloat(value.trim());
        if (!isNaN(value)) numericArray.push(value);
    });
    return numericArray;
};

const computeClasses = (geostatsObject, mode, numberOfClasses) => {

    switch (mode) {
        case MODE_QUANTILE:
            return geostatsObject.getClassQuantile(numberOfClasses);
        case MODE_INTERVAL:
            return geostatsObject.getClassEqInterval(numberOfClasses);
        case MODE_JENKS:
            return geostatsObject.getClassJenks(numberOfClasses);
        case MODE_GEOM:
            return geostatsObject.getClassGeometricProgression(numberOfClasses);
        case MODE_STD:
            return geostatsObject.getClassStdDeviation(numberOfClasses);
        default:
            return [];
    }
};


const ColorizeContainer = () => {

    const currentLayerId = useSelector((state) => state.currentLayerId);
    const layers = useSelector((state) => state.layers);
    const layout = useSelector((state) => state.layout);
    const dispatch = useDispatch();

    const [layerOptions, setSelectOptions] = useState([]);
    const [classificationOptions] = useState(getClassificationOptions());
    const [colorTypeOptions] = useState(getColorTypeOptions());
    const [colorBars, setColorBars] = useState([]);
    const [displayedColorBar, displayColorBar] = useState('');
    const [classificationMode, setClassificationMode] = useState(MODE_JENKS);
    const [numberOfClasses, setNumberOfClasses] = useState(5);
    const [selectedLayer, setSelected] = useState('noLayer');
    const [colorScaleType, setColorScaleType] = useState('Default');
    const [layoutProperties, showLayoutProperties] = useState('');
    const [vectorColorType, setVectorColorType] = useState(SINGLE_COLOR);
    const [showColorByValues, setValuesVisible] = useState(false);
    const [featureAttributes, setLayerAttributes] = useState([]);
    const [colorAttribute, setColorAttribute] = useState('');
    const [noDataValue, setNoDataValue] = useState('');
    const [opacity, setOpacity] = useState(0.8);
    const [rasterMin, setRasterMin] = useState(0);
    const [rasterMax, setRasterMax] = useState(100);
    const [currentColor, setCurrentColor] = useState('rgba(245,166,35,1)');
    const [colorPickerOpen, displayColorPicker] = useState(false);
    const [redBand, setRed] = useState(1);
    const [greenBand, setGreen] = useState(2);
    const [blueBand, setBlue] = useState(3);
    const [bands, setBandOptions] = useState([]);
    const [isSingleBand, setSingleBandProperties] = useState(true);
    const [message, setMessage] = useState('');
    const [removeNoData, setremoveNoData] = useState(false);

    useEffect(() => {
        const dropDownLayers = getLayerDropDownList(layers);
        setSelectOptions(dropDownLayers);
        if (currentLayerId) {
            setSelected(currentLayerId);
            const currentLayer = getLayerById(layers, currentLayerId);
            if (currentLayer) changeLayer(currentLayer);
        } else {
            setSelected('noLayer');
            setVectorColorType(SINGLE_COLOR);
            setValuesVisible(false);
            showLayoutProperties('');
        }
    }, [currentLayerId]);

    const updateRed = selected => {
        return setRed(selected.value);
    };
    const updateGreen = selected => {
        return setGreen(selected.value);
    };
    const updateBlue = selected => {
        return setBlue(selected.value);
    };
    const changeLayer = (currentLayer) => {

        Map.hideLegend();
        if (currentLayer.fileType === RASTER) {
            const properties = getRasterProperties(currentLayer);
            setRasterMin(properties['min']);
            setRasterMax(properties['max']);
            setNoDataValue(properties['noDataValue']);
            setremoveNoData(properties['removeNoData']);
            setOpacity(properties['opacity']);
            if (properties['numberOfBands'] > 2){
                setSingleBandProperties(false);
                setBandOptions(getListOfBands(properties['numberOfBands']));
            } else {
                setSingleBandProperties(true);
                setColorScaleType(properties['colorScaleType']);
                if (properties['colorScaleType'] !== 'Default') {
                    displayColorBar(generateColorBar(properties['colorScaleType']));
                }
            }
            setColorBars(getColorBars(RASTER));
            showLayoutProperties(RASTER);
        } else {
            if (!currentLayer.color) currentLayer.color = 'rgba(245,166,35,1)';
            setCurrentColor(currentLayer.color);
            if (vectorColorType === CATEGORY || vectorColorType === GRADUATED) {
                const attributes = Map.getAttributesForLayer(currentLayer.id);
                setLayerAttributes(attributes);
                setColorAttribute(attributes[0].value);
                if (colorScaleType === 'Default') setColorScaleType('Viridis');
                setColorBars(getColorBars(vectorColorType));
                displayColorBar(generateColorBar(colorScaleType));
            }
            showLayoutProperties(VECTOR);
        }

    };

    const selectLayer = selected => {

        const currentID = selected.value;
        if (currentID === 'noLayer') return;
        const currentLayer = getLayerById(layers, currentID);
        dispatch(updateSelectedLayer(currentLayer));
        setMessage('');
        Map.hideLegend();
        changeLayer(currentLayer);
        Map.zoomToLayer(currentLayer);
        return setSelected(currentID);

    };

    const changeVectorColorType = selected => {

        const option = selected.value;
        setMessage('');
        if (option === CATEGORY || option === GRADUATED) {
            let scaleType = colorScaleType;
            if (option === GRADUATED && colorScaleType === 'Default') {
                scaleType = 'Viridis';
                setColorScaleType(scaleType);
            }
            const attributes = Map.getAttributesForLayer(selectedLayer);
            setLayerAttributes(attributes);
            setColorAttribute(attributes[0].value);
            setValuesVisible(true);
            setColorBars(getColorBars(option));
            const colorBar = generateColorBar(scaleType);
            displayColorBar(colorBar);
        } else setValuesVisible(false);
        return setVectorColorType(option);
    };

    const changeColorAttributes = selected => {
        return setColorAttribute(selected.value);
    };
    const changeNumberOfClasses = valueAsNumber => {
        return setNumberOfClasses(valueAsNumber);
    };
    const applyVectorColors = () => {
        setMessage('');
        if (vectorColorType === CATEGORY) {
            const colorMap = {};
            let otherValues = false;
            let colors = [];
            let values;
            if (colorScaleType === 'Default') { //Random
                values = Map.getAttributeValues(selectedLayer, colorAttribute, true);
                if (values.length < 1) return;
                colors = randomColor({count: values.length});
            } else {
                const scaleType = getChromaScaleType(colorScaleType);
                values = Map.getAttributeValues(selectedLayer, colorAttribute, true);
                if (values.length < 1) return;
                // noinspection JSPotentiallyInvalidConstructorUsage
                const geostatsObject = new geostats(values);
                let min = geostatsObject.min();
                let max = geostatsObject.max();
                if (isNaN(min) || isNaN(max)) {
                    min = 0;
                    max = values.length;
                } else {
                    values = toNumericArray(values);
                    values.sort(function (a, b) {
                        return a - b;
                    });
                }
                colors = chroma.scale(scaleType).domain([min, max]).colors(values.length);
                if (colorScaleType === 'Spectral Reverse' || colorScaleType === 'Green-Red')
                    colors = colors.reverse();
            }
            const displayValues = [];
            values.forEach((v, index) => {
                if (!v || 0 === v.length) {
                    otherValues = true;
                    return;
                }
                displayValues.push(v);
                colorMap[v] = colors[index];
            });
            try {
                Map.updateVectorStyleByValue(selectedLayer, colorAttribute, colorMap);
            } catch (e) {
                setMessage('Warning: Errors occurred during rendering.');
            }
            Map.showLegend(selectedLayer, colorAttribute, colors, displayValues, otherValues);

        } else if (vectorColorType === GRADUATED) {
            const attributeValues = Map.getAttributeValues(selectedLayer, colorAttribute, false);
            const numericArray = toNumericArray(attributeValues);
            if (numericArray.length === 0) {
                setMessage('No numeric values found for classification!');
                return;
            } else if (numericArray.length < attributeValues.length) {
                setMessage('Non-numeric values have been removed.');
            }
            // noinspection JSPotentiallyInvalidConstructorUsage
            const geostatsObject = new geostats(numericArray);
            const max = geostatsObject.max();
            const min = geostatsObject.min();
            let classBreaks = [];
            let valid = true;
            try {
                classBreaks = computeClasses(geostatsObject, classificationMode, numberOfClasses);
            } catch (e) {
                valid = false;
            }
            classBreaks.forEach(value => {
                if (!value || isNaN(value)) valid = false;
            });
            if (!valid) {
                setMessage('Could not compute valid class breaks!');
                return;
            }
            let scaleType = getChromaScaleType(colorScaleType);
            let colors = chroma.scale(scaleType).domain([min, max]).colors(numberOfClasses);
            if (colorScaleType === 'Spectral Reverse' || colorScaleType === 'Green-Red')
                colors = colors.reverse();
            const displayValues = [];
            const lang = 'en-US';
            colors.push(colors[colors.length - 1]);
            for (let i = 0; i < numberOfClasses; i++) {
                let prefix = '';
                if (i > 0) prefix = '> ';
                const from = Number(classBreaks[i].toFixed(2)).toLocaleString(lang);
                const to = Number(classBreaks[i + 1].toFixed(2)).toLocaleString(lang);
                const text = prefix + from + ' - ' + to;
                displayValues.push(text);
            }

            Map.hideLegend();
            try {
                Map.updateVectorStyleByClass(selectedLayer, colorAttribute, colors, classBreaks);
            } catch (e) {
                setMessage('Warning: Errors occurred during rendering.');
            }
            Map.showLegend(selectedLayer, colorAttribute, colors, displayValues, 0);

        } else if (vectorColorType === SINGLE_COLOR) {
            Map.hideLegend();
            const currentLayer = getLayerById(layers, currentLayerId);
            currentLayer.color = currentColor;
            const style = Map.getVectorStyle(currentColor);
            Map.updateVectorStyle(selectedLayer, style);
        }
    };
    const changeClassificationMode = selected => {
        const currentMode = selected.value;
        return setClassificationMode(currentMode);
    };

    const changeColorBar = selected => {
        const currentScale = selected.value;
        if (currentLayerId) {
            const colorBar = generateColorBar(currentScale);
            displayColorBar(colorBar);
        }
        return setColorScaleType(currentScale);
    };

    const updateOpacity = (event, value) => {
        return setOpacity(value);
    };

    const toggleRemoveNoData = () => {
        return setremoveNoData(!removeNoData);
    };
    const colorize = () => {
        return colorizeRaster(layers, currentLayerId, colorScaleType, opacity, rasterMin, rasterMax, redBand, greenBand, blueBand, removeNoData);
    };
    const toggleColorPicker = () => {
        return displayColorPicker(!colorPickerOpen);
    };
    const changeColor = color => {
        const rgba = `rgba(${color.rgb.r},${color.rgb.g},${color.rgb.b}, ${color.rgb.a})`;
        return setCurrentColor(rgba);
    };

    return ColorizeComponent({
        selectedLayer, layoutProperties, selectLayer, layerOptions, changeColorBar, colorBars,
        colorScaleType, updateOpacity, colorize, displayedColorBar, rasterMin, rasterMax, noDataValue,
        showColorByValues, vectorColorType, colorTypeOptions, changeVectorColorType, changeClassificationMode,
        classificationMode, classificationOptions, numberOfClasses, changeNumberOfClasses, featureAttributes,
        changeColorAttributes, colorAttribute, applyVectorColors, currentColor, colorPickerOpen, toggleColorPicker,
        changeColor, message, isSingleBand, updateRed, updateGreen, updateBlue, bands, redBand, blueBand, greenBand,
        removeNoData, toggleRemoveNoData, opacity, layout,
    });

};

export default ColorizeContainer;
