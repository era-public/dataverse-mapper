import React from 'react';
import ToolHeader from '../../tool-elements/tool-header';
import ToolContent from '../../tool-elements/tool-content';
import Slider from '@material-ui/core/Slider';
import NumericInput from 'react-numeric-input';
import LayerSelectionComponent from '../../layer-selection';
import VectorColorComponent from './vector-color-picker.component';
import { GRADUATED, RASTER, SINGLE_COLOR, VECTOR,
} from '../../../constants/variables';
import ToolFooter from "../../tool-elements/tool-footer";
import MultiBandComponent from "./multi-band.component";
import Select from "react-select";
import {FormControlLabel, Switch, Table, TableBody, TableCell, TableRow} from '@material-ui/core';
import {customSelectStyle} from "../../tool-elements/tool-content/tool-content.component";

const mapColors = (color) => {
    return (
        <i id={color.key} key={color.key} className="gradientStep" style={{background: color.value}}/>
    );
};

const ColorizeComponent = ({   selectedLayer, layoutProperties, selectLayer, layerOptions, changeColorBar, colorBars,
                               colorScaleType, updateOpacity, colorize, displayedColorBar, rasterMin, rasterMax, noDataValue,
                               showColorByValues, vectorColorType, colorTypeOptions, changeVectorColorType, changeClassificationMode,
                               classificationMode, classificationOptions, numberOfClasses, changeNumberOfClasses, featureAttributes,
                               changeColorAttributes, colorAttribute, applyVectorColors, currentColor, colorPickerOpen, toggleColorPicker,
                               changeColor, message, isSingleBand, updateRed, updateGreen, updateBlue, bands, redBand, blueBand, greenBand,
                                removeNoData, toggleRemoveNoData, opacity, layout,
                           }) => (
    <div id='colorize-tool' className={layout === 'hide-menu' ? 'tool-hidden' : 'tool'}>
        <ToolHeader
            iconClass='style-tool-icon-white'
            title="Adjust Colors"
        />
        <ToolContent className={'colorize-height'}>

            <LayerSelectionComponent
                selectedLayer={selectedLayer}
                selectLayer={selectLayer}
                options={layerOptions}/>
            <br/>

            {layoutProperties === RASTER &&
            <div>
                {isSingleBand &&
                <div>
                    <Select styles={customSelectStyle}
                        onChange={changeColorBar}
                        value={colorBars.filter(option => {
                            return option.value === colorScaleType;
                        })}
                        options={colorBars}
                    />
                    <br/>

                    {
                        colorScaleType !== 'Default' &&
                            <div className="gradient inner">

                                {displayedColorBar.map(mapColors)}

                                <span className="domain-min smallest-text">{rasterMin.toPrecision(3)}</span>
                                <span
                                    className="domain-med smallest-text">{((rasterMin + rasterMax) * 0.5).toPrecision(3)}</span>
                                <span className="domain-max smallest-text">{rasterMax.toPrecision(3)}</span>
                            </div>

                    }

                </div>
                }
                {!isSingleBand &&
                <MultiBandComponent
                    updateRed={updateRed}
                    updateGreen={updateGreen}
                    updateBlue={updateBlue}
                    bands={bands}
                    redBand={redBand}
                    blueBand={blueBand}
                    greenBand={greenBand}
                />
                }
                <div>
                    <br/>
                    <Table>
                        <TableBody>
                            <TableRow>
                                <TableCell className="smaller-text"><strong>Minimum pixel value:</strong></TableCell>
                                <TableCell className="smaller-text">{rasterMin.toPrecision(3)}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell className="smaller-text"><strong>Maximum pixel value:</strong></TableCell>
                                <TableCell className="smaller-text">{rasterMax.toPrecision(3)}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell className="smaller-text"><strong>No Data value:</strong></TableCell>
                                <TableCell className="smaller-text">{noDataValue}</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                    <br/>
                    <div className="table-row-font smaller-text no-border"><strong>Opacity:</strong></div>
                    <Slider defaultValue={0.8}
                            value={opacity}
                            max={1}
                            step={0.1}
                            valueLabelDisplay="on"
                            onChangeCommitted={updateOpacity}
                    />
                    <FormControlLabel
                        control={
                            <Switch
                                checked={removeNoData || false}
                                onChange={toggleRemoveNoData}
                                inputProps={{ 'aria-label': 'controlled' }}
                            />
                        }
                        label="Hide NoData values" />

                    <div className='content-row submit-row'>
                        <button
                            className='gt-button-accent full'
                            onClick={colorize}
                        >
                            Apply changes
                        </button>
                    </div>

                </div>

            </div>}


            {layoutProperties === VECTOR &&
            <div>
                <Select styles={customSelectStyle}
                    onChange={changeVectorColorType}
                    value={colorTypeOptions.filter(option => {
                        return option.value === vectorColorType;
                    })}
                    options= {colorTypeOptions}
                    isSearchable={false}
               />

                {!showColorByValues &&
                <div>
                    <br/>
                    <VectorColorComponent
                        currentColor={currentColor}
                        colorPickerOpen={colorPickerOpen}
                        toggleColorPicker={toggleColorPicker}
                        changeColor={changeColor}
                    />
                </div>
                }
                {vectorColorType !== SINGLE_COLOR &&

                <div>
                    <br/>
                    <Select styles={customSelectStyle}
                        onChange={changeColorBar}
                        value={colorBars.filter(option => {
                            return option.value === colorScaleType;
                        })}
                        options={colorBars}
                    />
                    {colorScaleType === 'Default' && <br/>}
                    {
                        colorScaleType !== 'Default' && displayedColorBar &&
                        <div className="outer">
                            <div className="gradient inner top-margin">
                                {displayedColorBar.map(mapColors)}
                            </div>
                        </div>
                    }
                    <Select styles={customSelectStyle}
                        value={featureAttributes.filter(option => {
                            return option.value === colorAttribute;
                        })}
                        onChange={changeColorAttributes}
                        options={featureAttributes}
                    />
                    {vectorColorType === GRADUATED &&
                        <div>
                            <br/>
                            <Select styles={customSelectStyle}
                                isSearchable={false}
                                onChange={changeClassificationMode}
                                value={classificationOptions.filter(option => {
                                    return option.value === classificationMode;
                                })}
                                options={classificationOptions}
                            />
                            <br/>
                            <div className='classes-line'>Classes:
                                <NumericInput
                                    onChange={changeNumberOfClasses}
                                    min={1}
                                    max={100}
                                    value={numberOfClasses}/>
                            </div>
                        </div>
                    }

                </div>
                }

                {message !== '' &&
                <div className='content-row'>
                    {vectorColorType === GRADUATED && <div><br/><br/></div>}
                    <span className='warning full-width'>{message}</span>
                </div>
                }

                <div className='content-row submit-row'>
                    <button
                        className='gt-button-accent full'
                        onClick={applyVectorColors}
                    >
                        Apply changes
                    </button>
                </div>
            </div>
            }
        </ToolContent>
        <ToolFooter/>
    </div>
);

export default ColorizeComponent;
