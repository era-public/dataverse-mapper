import React from 'react';
import {SketchPicker} from 'react-color';

const VectorColorComponent = ({currentColor, colorPickerOpen, toggleColorPicker, changeColor}) => (

    <div id='color-picker'>

        <button className="colorbutton" style={{backgroundColor: currentColor}} onClick={toggleColorPicker}>
            Color: {currentColor}

        </button>
        {colorPickerOpen ? <div className="popover">
            <div className="cover" onClick={toggleColorPicker}/>
            <SketchPicker color={currentColor} onChange={changeColor}/>
        </div> : null}

    </div>
);
export default VectorColorComponent;
