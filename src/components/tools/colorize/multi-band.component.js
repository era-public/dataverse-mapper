import React from 'react';
import Select from "react-select";
import {customSelectStyle} from "../../tool-elements/tool-content/tool-content.component";

const bandTextColor = (color) => {
    return (theme) => ({
        ...theme,
        colors: {
            ...theme.colors,
            neutral80: color,
        },
    });
};

const MultiBandComponent = ({updateRed, updateGreen, updateBlue, bands, redBand, blueBand, greenBand}) => (
    <div>
        <Select styles={customSelectStyle}
                theme={bandTextColor('red')}
                isSearchable={false}
                onChange={updateRed}
                value={bands.filter(option => {
                    return option.value === redBand;
                })}
                options={bands}
        />
        <br/>
        <Select styles={customSelectStyle}
                theme={bandTextColor('green')}
                isSearchable={false}
                onChange={updateGreen}
                value={bands.filter(option => {
                    return option.value === greenBand;
                })}
                options={bands}
        />
        <br/>
        <Select styles={customSelectStyle}
                theme={bandTextColor('blue')}
                isSearchable={false}
                onChange={updateBlue}
                value={bands.filter(option => {
                    return option.value === blueBand;
                })}
                options={bands}
        />
    </div>
);

export default MultiBandComponent;
