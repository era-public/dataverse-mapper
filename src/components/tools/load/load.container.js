import LoadComponent from './load.component';
import {addGeoJSON, addRaster, addVectorData} from '../../../actions/layer-actions';
import { useSelector, useDispatch } from 'react-redux';
import {useEffect, useRef, useState} from "react";
import {showAlert} from '../../../actions/alert-actions';
import {readString, readRemoteFile} from 'react-papaparse';
import GeoJSON from 'geojson';
import {CSVFILE} from "../../../constants/variables";
import FileService from "../../../services/FileService";
import {startLoading, stopLoading} from "../../../actions/loading-actions";

const urlIsValid = url => {
    return /^http|^https/.test(url);
};

const getFileID = file => {

    let id = Date.now();
    if (typeof file !== 'object' && file.includes('fileID')) {
        const params = (new URL(file)).searchParams;
        id = params.get('fileID') + '_' + id;
    }
    return id;
};

const isValidNumber = (value) => {
    if (!value) return false;
    return !isNaN(value);
};

const csvToGeoJSON = (results, fileName, fileID, addGeoJSON, displayCSVColumns, csvColumnsVisible, setColumnOptions,
                      latitude, longitude, setLoadMessage, setButtonLabel, numberOfFiles, dispatch) => {

    const columns = results.meta.fields.map(function (value) {
        return value.toUpperCase();
    });
    let data = results.data.map(item => {
        return Object.fromEntries(
            Object.entries(item).map(([k, v]) => [k.toUpperCase(), v]),
        );
    });

    if (columns.includes(latitude) && columns.includes(longitude)) {
        setButtonLabel('Load');
        const numberOfRows = data.length;
        data = data.filter(function (item) {
            return isValidNumber(item[latitude]) && isValidNumber(item[longitude]);
        });
        if (data.length === 0) {
            setLoadMessage({status: 'ERROR', message: 'No valid coordinates could be extracted!'});
            return;
        } else if (numberOfRows - data.length > 0){
            setLoadMessage({status: 'WARNING', message: 'Invalid rows skipped: ' + String(numberOfRows - data.length)});
        }

        GeoJSON.parse(data, {Point: [latitude, longitude]},  geojson => {
            dispatch(addGeoJSON(geojson, fileName, fileID));
        });

        return true;

    } else {

        if (numberOfFiles > 1) return false;

        const csvColumns = [];
        results.meta.fields.forEach(function (value) {
            csvColumns.push({'value': value, 'label': value});
        });
        setColumnOptions(csvColumns);
        displayCSVColumns(true);
        setButtonLabel('Reload');

        return true;
    }

};
const processRemoteURL = (urlInput, apiKey, fileID, addGeoJSON, displayCSVColumns, csvColumnsVisible, setColumnOptions, latitude,
                          longitude, setLoadMessage, setButtonLabel, addVectorData, addRaster, showAlert, dispatch) => {

    // if a Dataverse link is used, we can add the API token if available
    if ((urlInput.includes(process.env.REACT_APP_DATAVERSE_URL) || urlInput.includes(process.env.REACT_APP_ALT_DATAVERSE_URL))
        && !urlInput.includes('key=')){
        if (apiKey) {
            urlInput = new URL(urlInput);
            urlInput.searchParams.append('key', apiKey);
            urlInput = urlInput.href;
        }
    }
    FileService.getDataverseFileType(urlInput, fileID, dispatch).then(fileMetadata => {
        const fileType = fileMetadata.fileType;
        const fileName = fileMetadata.fileName;
        if (fileType === CSVFILE) {
            return readRemoteFile(urlInput, {
                header: true, dynamicTyping: true, download: true, complete: (results) => {
                    csvToGeoJSON(results, fileName, fileID, addGeoJSON, displayCSVColumns, csvColumnsVisible,
                        setColumnOptions, latitude, longitude, setLoadMessage, setButtonLabel, 1, dispatch);
                    dispatch(stopLoading());
                }, error: () => {
                    dispatch(stopLoading());
                    dispatch(showAlert('Error fetching remote CSV file (no permission?)'));
                },
            });
        } else {
            displayCSVColumns(false);
            return FileService.processFileType(fileMetadata, addVectorData, addRaster, dispatch).then(result => {
                dispatch(stopLoading());
                if (!result || result !== 'SUCCESS') {
                    dispatch(showAlert(`${process.env.REACT_APP_TITLE} was unable to load the file.`));
                }
            });
        }
    }).catch((e) => {
        dispatch(stopLoading());
        dispatch(showAlert(e));
    });
};
const usePrevious = (value) => {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    }, [value]);
    return ref.current;
};

const LoadContainer = () => {

    const apiKey = useSelector((state) => state.apiKey);
    const layout = useSelector((state) => state.layout);
    const dispatch = useDispatch();

    const [urlInput, setURLInput] = useState('');
    const [inputURLKey, setInputURLKey] = useState('inputURL');
    const [fileInput, setFileInput] = useState([]);
    const [skippedFiles, setSkippedFiles] = useState(0);
    const [processedFiles, setProcessedFiles] = useState(0);
    const [numberOfFiles, setNumberOfFiles] = useState(0);
    const [latitude, setLatitude] = useState('LATITUDE');
    const [longitude, setLongitude] = useState('LONGITUDE');
    const [csvColumnsVisible, displayCSVColumns] = useState(false);
    const [columns, setColumnOptions] = useState([]);
    const [loadMessage, setLoadMessage] = useState({});
    const [loadButtonLabel, setButtonLabel] = useState('Load');
    const prevSkipped = usePrevious(skippedFiles);

   /* useEffect(() => {
        if (requestedFile){
            setURLInput(requestedFile);
            dispatch(removeRequestedURL());
            dispatch(startLoading('Loading File'));
            processRemoteURL(requestedFile, apiKey, getFileID(requestedFile), addGeoJSON, displayCSVColumns, csvColumnsVisible,
                setColumnOptions, latitude, longitude, setLoadMessage, setButtonLabel, addVectorData, addRaster, showAlert, dispatch);
        }
    }, [requestedFile]); */

    // noinspection CommaExpressionJS
    useEffect(() => {
        if (prevSkipped === 0 && skippedFiles > 0){
            setLoadMessage({status: 'WARNING', message: 'Not all files could be loaded. ' +
                            'Please make sure your files are valid and try to re-load them separately.'});
        }
    }), [skippedFiles];

    // noinspection CommaExpressionJS
    useEffect(() => {
        if (processedFiles > 0 && processedFiles === numberOfFiles){
            dispatch(stopLoading());
        }
    }), [processedFiles];

    const updateURLInput = event => {
        return setURLInput(event.target.value);
    };
    const updateFileInput = files => {
        return setFileInput(files);
    };
    const updateLatitude = selected => {
        return setLatitude(selected.value.toUpperCase());
    };
    const updateLongitude = selected => {
        return setLongitude(selected.value.toUpperCase());
    };
    const reset = () => {
        setURLInput('');
        setFileInput([]);
        setInputURLKey(Date.now());
        displayCSVColumns(false);
        setLoadMessage({status: 'INFO', message: ''});
    };
    const loadFile = () => {

        let isRemoteURL = false;
        if (urlInput !== '') {
            if (!urlIsValid(urlInput)) {
                return dispatch(showAlert('Please make sure you are using a valid URL to a supported data format.'));
            } else isRemoteURL = true;
        } else if (!fileInput || fileInput.length === 0) {
            return dispatch(showAlert('Please add either a valid URL or a local file!'));
        }
        setLoadMessage({status: 'INFO', message: ''});
        if (isRemoteURL) {
            dispatch(startLoading('Loading File'));
            const fileID = getFileID(fileInput);
            processRemoteURL(urlInput, apiKey, fileID, addGeoJSON, displayCSVColumns, csvColumnsVisible, setColumnOptions,
                latitude, longitude, setLoadMessage, setButtonLabel, addVectorData, addRaster, showAlert, dispatch);
        } else {
            const numberOfFiles = fileInput.length;
            const message = numberOfFiles > 1 ? 'Loading Files' : 'Loading File';
            setNumberOfFiles(numberOfFiles);
            setSkippedFiles(0);
            setProcessedFiles(0);
            dispatch(startLoading(message));
            let processed = 0;
            fileInput.forEach(file => {

                const fileID = getFileID(file);
                const fileName = file.name;
                const fileType = FileService.getFileType(fileName);
                if (fileType === CSVFILE) {
                        try {
                            const fileReader = new FileReader();
                            fileReader.onload = evt => {
                                readString(evt.target.result, {
                                    header: true, dynamicTyping: true, complete: (results) => {
                                        let fileLoaded = csvToGeoJSON(results, fileName, fileID, addGeoJSON, displayCSVColumns, csvColumnsVisible,
                                            setColumnOptions, latitude, longitude, setLoadMessage, setButtonLabel, numberOfFiles, dispatch);
                                        if (!fileLoaded) setSkippedFiles(skippedFiles+1);
                                        processed += 1;
                                        setProcessedFiles(processed);
                                    },
                                });
                            };
                            fileReader.readAsText(file);
                        } catch (e) {
                            processed += 1;
                            setProcessedFiles(processed);
                            if (numberOfFiles === 1) dispatch(showAlert('The CSV file you tried to load is not valid. Please try again with a different file.'));
                            else setSkippedFiles(skippedFiles+1);
                        }
                } else {
                    displayCSVColumns(false);
                    const fileObj = {
                        input: file,
                        fileType: fileType,
                        fileID: fileID,
                        fileName: fileName,
                        isRemoteURL: isRemoteURL,
                    };

                    FileService.processFileType(fileObj, addVectorData, addRaster, dispatch).then(result => {
                        processed += 1;
                        setProcessedFiles(processed);
                        if (!result || result !== 'SUCCESS') {
                            if (numberOfFiles === 1) {
                                 if (result.toString().includes('no layers'))
                                     result = 'No geospatial layers found!';
                                 dispatch(showAlert(`File could not be loaded: ${result}`));
                            } else setSkippedFiles(skippedFiles + 1);
                        }
                    });

                }
            });

        }

    };

    return LoadComponent({updateURLInput, updateFileInput, urlInput,
        loadFile, csvColumnsVisible, updateLatitude, updateLongitude,
        columns, loadMessage, loadButtonLabel, reset, fileInput, inputURLKey, layout});

};

export default LoadContainer;
