import React from 'react';
import Select from 'react-select';
import ToolHeader from '../../tool-elements/tool-header';
import ToolContent from '../../tool-elements/tool-content';
import ToolFooter from '../../tool-elements/tool-footer';
import Dropzone from 'react-dropzone';
import InfoPopover from "../../popover";
import {smallCustomSelectStyle} from "../../tool-elements/tool-content/tool-content.component";

const renderLoadMessage = (option) => {
    switch (option.status){
        case 'ERROR':
            return ( <p className='error-message'>{option.message}</p> );
        case 'WARNING':
            return (<p className='warning'>{option.message} </p> );
      default:
          return ( <p> {option.message} </p> );
    }
};

const LoadComponent = ({   updateURLInput, updateFileInput, urlInput, loadFile, csvColumnsVisible, updateLatitude, updateLongitude,
                           columns, loadMessage, loadButtonLabel, reset, inputURLKey, fileInput, layout,
                       }) => (
    <div id='load-tool' className={layout === 'hide-menu' ? 'tool-hidden' : 'tool'}>
        <ToolHeader
            iconClass = "load-tool-icon-white"
            title="Load Files"
        />
        <ToolContent>
            <div className='tool-desc float-left'>
                You can add geodata to the map by providing a URL or uploading a local file.<br/><br/>
            <b>Supported file types are GeoTiff, zipped Shapefile, GeoJSON, KML, and CSV with lat/lon coordinates.</b>
                <InfoPopover className={'float-right csv-info-button'} content={<div className='small-padding popover-content'>
                    <span className='smallest-text'>
                        For CSV files, coordinates are expected in columns called LATITUDE and LONGITUDE by default.
                    </span>
            </div>}/><p className={'clear-float'}/>
            </div>
            <div className="input-group">
                <input
                    type="text"
                    key={inputURLKey}
                    className="gt-input"
                    placeholder='URL to Your File'
                    defaultValue={urlInput}
                    onChange={updateURLInput}
                />
            </div>
            <p className="or"><b>OR</b></p>

            <Dropzone accept={'image/tiff, .tif, .geotiff, .geojson, .geotif, .kml, .tab, application/zip, ' +
            'application/geo+json, text/csv, .csv, .zip, application/vnd.google-earth.kml+xml'} onDrop={acceptedFiles => {
                updateFileInput(acceptedFiles);
            }}>
                {({getRootProps, getInputProps, isDragActive}) => (
                    <section {...getRootProps()} className="files-container">
                        <input {...getInputProps()} />
                        {
                            isDragActive ?
                                <p>Drop your files here ...</p> :
                                <p> Click to select a file, or drag and drop files here.
                                    <br/>
                                </p>
                        }
                            <ul> {
                                !isDragActive &&
                                fileInput.length > 0 &&
                                fileInput.map((file, index) => (
                                <li key={file.path + index}>
                                    {file.path}
                                </li>))
                                }
                            </ul>
                    </section>
                )}
            </Dropzone>

            <button className={'underline-button small-spacing'}
                    onClick={reset}
            >
                Reset Input
            </button>

            <br/><br/>
            {csvColumnsVisible &&
            <div>
                <p className="warning">
                      Please select <strong>Latitude / Longitude</strong> columns with valid geographic coordinates.</p>

                <Select styles={smallCustomSelectStyle}
                    placeholder='Select Latitude field'
                    onChange={updateLatitude}
                    options={columns}
                    maxMenuHeight={150}
                />
            </div>
          }
          {csvColumnsVisible &&
          <br/>}

          {csvColumnsVisible &&
          <Select styles={smallCustomSelectStyle}
              placeholder='Select Longitude field'
              onChange={updateLongitude}
              options={columns}
              maxMenuHeight={150}
          />
          }
          {renderLoadMessage(loadMessage)}
            <div className="content-row submit-row">
                <button
                    className='gt-button-accent full'
                    onClick={loadFile}
                >
                    {loadButtonLabel}
                </button>
            </div>
        </ToolContent>
        {!csvColumnsVisible && <ToolFooter/>}
    </div>
);

export default LoadComponent;
