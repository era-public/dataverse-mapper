import { useSelector, useDispatch } from 'react-redux';
import FileService from "../../../services/FileService";
import {addGeoJSON, addVectorData, addRaster} from "../../../actions/layer-actions";
import {clearResults, setResults, updateResultItem, updateResults} from "../../../actions/results-actions";
import {useEffect, useRef, useState} from "react";
import {startLoading, stopLoading} from "../../../actions/loading-actions";
import {CSVFILE, UNSUPPORTED, ZIPPED_FILE} from "../../../constants/variables";
import SearchComponent from "./search.component";
import {startDrawing, stopDrawing} from "../../../actions/drawing-actions";
import {updateBoundingBox, removeBoundingBox} from "../../../actions/bb-actions";
import Map from "../../../map/Map";
import {removeSearchEnvelope} from "../../../actions/search-envelope-actions";

const formatSearchItem = k => {

    let key = k;
    if (key === 'name_of_dataverse') key = 'Dataverse';
    else if (key === 'published_at') key = 'Published';
    else if (key === 'dataset_citation') key = 'Dataset Citation';
    else if (key === 'dataset_name') key = 'Dataset Name';
    else if (key === 'file_type') key = 'File Type';
    else if (key === 'size_in_bytes') key = 'File Size';
    else if (key === 'fileCount') key = 'Files';
    else if (key === 'global_id') key = 'ID';
    else if (key === 'geographicCoverage') key = 'Coverage';
    else if (key === 'versionState') key = 'Status';
    else if (key === 'majorVersion') key = 'Version';
    key = key.charAt(0).toUpperCase() + key.slice(1);

    return key;
};

const mapFileToMap = (url, fileName, fileID, contentType, apiKey, addVectorData, addRaster, setLoadFileInfo, dispatch, splitView, toggleSearchPageDisplay) => {
    setLoadFileInfo({status: 'INFO', message: ''});
    let fileUrl =  process.env.REACT_APP_DATAVERSE_URL + '/api/v1/access/datafile/' + fileID + '?gbrecs=true';
    let headers = apiKey ? {'X-Dataverse-key': apiKey} : {};
    fetch(fileUrl, {method: 'HEAD', headers: headers}).then(res => {
        if (res.status === 403 || res.status === 401) {
            setLoadFileInfo({status: 'WARNING', message: `${fileName} could not be mapped. (No permission!)`});
            return;
        } else if (res.status !== 200) {
            setLoadFileInfo({status: 'WARNING', message: 'Error when loading URL!'});
            return;
        }
        dispatch(startLoading(`Loading file...`));
        if (apiKey) fileUrl = fileUrl + '&key=' + apiKey;
        fileID = fileID + '_ ' + Date.now();
        const fileObj = {input: fileUrl, fileType: contentType, fileID: fileID, fileName: fileName, isRemoteURL: true};
        return FileService.processFileType(fileObj, addVectorData, addRaster, dispatch).then(result => {
            dispatch(stopLoading());
            if (!splitView) toggleSearchPageDisplay();
            if (result !== 'SUCCESS') {
                setLoadFileInfo({status: 'WARNING', message: `${fileName} could not be mapped. ${result}`});
            }
        });
    });

};

const batchFileMapping = (filesToLoad, apiKey, addVectorData, addRaster, dispatch) => {

    let count = filesToLoad.length;
    let currentFile = 1;
    return new Promise((resolve) => {
        let errorLog = [];
        dispatch(startLoading(`Loading file ${currentFile}/${filesToLoad.length}...`));
        filesToLoad.forEach(item => {
            const url = item.url;
            const fileName = item.fileName;
            const fileID = item.fileID;
            let fileUrl = url + '?gbrecs=true';
            let contentType = item.contentType;
            if (apiKey) fileUrl = fileUrl + '&key=' + apiKey;
            contentType = FileService.getFileType(contentType);
            fetch(fileUrl, {method: 'head'}).then(res => {
                let access = true;
                if (res.status === 403 || res.status === 401) {
                    errorLog.push(`${fileName} (no permission)`);
                    access = false;
                } else if (res.status !== 200) {
                    errorLog.push(`${fileName} (load error)`);
                    access = false;
                }
                if (!access){
                    currentFile+=1;
                    if (currentFile>count) resolve(errorLog);
                    return;
                }
                const fileObj = {
                    input: fileUrl,
                    fileType: contentType,
                    fileID: fileID,
                    fileName: fileName,
                    isRemoteURL: true,
                };
                FileService.processFileType(fileObj, addVectorData, addRaster, dispatch).then(result => {
                    if (result !== 'SUCCESS') {
                        errorLog.push(`${fileName} (${result})`);
                        dispatch(startLoading(`File ${currentFile} not mappable! Skipping...`));
                    }
                    currentFile+=1;
                    if (currentFile>count) resolve(errorLog);
                    else dispatch(startLoading(`Loading file ${currentFile}/${filesToLoad.length}...`));
                });
            });
        });
    });

};

const getSortOptions = () => {
    return [
        {value: 'nameA', label: 'Name (A - Z)'},
        {value: 'nameZ', label: 'Name (Z - A)'},
        {value: 'newest', label: 'Publication date (newest first)'},
        {value: 'oldest', label: 'Publication date (oldest first)'},
    ];
};

/**
 * Searches with given properties and extracts results.
 * */
const searchDataverse = (props, setResultsMessage, apiKey) => {
    return new Promise((resolve, reject) => {

        let filterQuery = '';
        let queryTerm = props.searchTerm;
        props.typeFilters.forEach(filter => {
            if (filter.value === 'files') {
                filterQuery+='&type=file';
            } else if (filter.value === 'datasets') {
                filterQuery+='&type=dataset';
            } else if (filter.value === 'dataverses') {
                filterQuery+='&type=dataverse';
            } else if (filter.value === 'geospatial') {
                if (queryTerm.trim() === '')
                    queryTerm = 'fileTags:Geospatial';
                else queryTerm += '%20AND%20fileTags:Geospatial';
            } else if (filter.value === 'geospatial_csv'){
                if (queryTerm.trim() === '')
                    queryTerm = 'tabularDataTag:Geospatial';
                else queryTerm += '%20AND%20tabularDataTag:Geospatial';
            }
        });
        let countryQuery = '';
        props.countryFilters.forEach(filter => {
            if (countryQuery === '') countryQuery = 'country:"'+filter.label+'"';
            else countryQuery += '+OR+country:"'+filter.label+'"';
        });

        let datasetQuery = '';
        props.datasetFilters.forEach(id => {
            if (datasetQuery === '') datasetQuery = 'dsPersistentId:"'+id+'"';
            else datasetQuery += '+OR+dsPersistentId%3A"'+id+'"';
        });

        let dataverseQuery = '';
        props.dataverseFilters.forEach(dataverse => {
            dataverseQuery += '&subtree='+dataverse.value;
        });

        if (queryTerm.trim() === '') queryTerm = '*';
        if (countryQuery !== '') queryTerm+='+AND+'+countryQuery;
        if (datasetQuery !== '') queryTerm+='+AND+('+datasetQuery+')';
        let searchURL = process.env.REACT_APP_DATAVERSE_URL + '/api/v1/search?q=' + queryTerm + '&start=' + props.offset + filterQuery + dataverseQuery;
        if (props.sortOrder) {
            if (props.sortOrder === 'nameA') searchURL += '&sort=name&order=asc';
            else if (props.sortOrder === 'nameZ') searchURL += '&sort=name&order=desc';
            else if (props.sortOrder === 'newest') searchURL += '&sort=date&order=desc';
            else if (props.sortOrder === 'oldest') searchURL += '&sort=date&order=asc';
        }
        let headers = apiKey ? {'X-Dataverse-key': apiKey} : {};
        fetch(searchURL, {method: 'GET', headers: headers})
            .then(res => res.json())
            .then(json => {
                if (json.status === 'OK'){
                    const data = json.data;
                    const numberOfResults = data['total_count'];
                    let totalPages = 1;
                    if (props.offset === 0) {
                        if (numberOfResults > 10) {
                            totalPages = Math.ceil(numberOfResults / 10);
                        }
                        props.pages = totalPages;
                        if (numberOfResults === 0 && props.countryFilters.length > 0){
                            setResultsMessage({status: 'info', totalResults: 0, message: `Total results: 0`,
                                subMessage: `Please note that filtering by country will only return results 
                                if respective geospatial metadata is available for at least one dataset.`});
                        } else setResultsMessage({status: 'info', totalResults: numberOfResults, message: `Total results: ${numberOfResults}`});
                    }
                    props.totalCount = numberOfResults;
                    props.items = data.items;
                    resolve(props);
                } else {
                    let error = json.message;
                    if (json.message.includes('X-Dataverse-key'))
                        error = `No permission (no valid API key for ${process.env.REACT_APP_DATAVERSE_URL})`;
                    setResultsMessage({status: 'error', message: `An error occurred (${error})`});
                    reject('ERROR');
                }
            }).catch(error => {
                setResultsMessage({status: 'error', message: `An error occurred (${error})`});
                reject(error);
            });
    });
};

/**
 * Analyses search results and prepares them for display. Dispatches final list of results to store.
 * */
const listResults = (searchSettings, apiKey, dispatch) => {

    const currentResults = [];
    searchSettings.items.forEach((item, index) => {
        const displayItem = {};
        if (item.type === 'dataset') {
            displayItem.editable = false;
            displayItem.geospatialMetadataEnabled = false;
            displayItem.isDraft = false;
            let versionPath;
            let status = item['versionState'];
            let uniqueUpdateKey = item['global_id']+'_'+status;
            displayItem.key = uniqueUpdateKey;
            if (status === 'DRAFT') {
                versionPath = 'versions/:draft';
            } else versionPath = 'versions/:latest-published';
            const datasetMetadataURL = process.env.REACT_APP_DATAVERSE_URL + '/api/v1/datasets/:persistentId/' + versionPath + '?persistentId=' +
                encodeURIComponent(item['global_id']);
            let headers = apiKey ? {'X-Dataverse-key': apiKey} : {};
            fetch(datasetMetadataURL,
                {method: 'GET', headers: headers})
                .then(res => res.json())
                .then(json => {
                    if (json.status === 'ERROR' || !json.data){
                        return;
                    }
                    let jsonData = json.data;
                    let metadataBlocks = jsonData['metadataBlocks'];
                    if (jsonData['versionState'] === 'DRAFT')
                        dispatch(updateResultItem({key: uniqueUpdateKey, property: 'isDraft', value: true}));
                    if (metadataBlocks && metadataBlocks['geospatial']) {
                        displayItem.geospatialMetadataEnabled = true;
                        dispatch(updateResultItem({key: uniqueUpdateKey, property: 'geospatialMetadataEnabled', value: true}));
                        if (jsonData['datasetId']){ //check if bounding boxes are editable by user
                            displayItem.datasetId = jsonData['datasetId'];
                            dispatch(updateResultItem({key: uniqueUpdateKey, property: 'datasetId', value: jsonData['datasetId']}));
                            fetch('https://data.goettingen-research-online.de/api/v1/datasets/' + jsonData['datasetId'] + '/assignments',
                                {method: 'GET', headers: headers})
                                .then(res => res.json())
                                .then(json => {
                                    if (json.status === 'OK'){
                                        dispatch(updateResultItem({key: uniqueUpdateKey, property: 'editable', value: true}));
                                    }
                                });
                        }
                        displayItem.coverage = [];
                        displayItem.boundingBoxes = [];
                        metadataBlocks['geospatial'].fields.forEach(field => {
                            if (field.typeName === 'geographicCoverage'){
                                displayItem.coverage = field.value;
                            } else if (field.typeName === 'geographicBoundingBox'){
                                let textArray = [];
                                let geojsonArray = [];
                                field.value.forEach(box => {
                                    let eastLon = box.eastLongitude.value || 180;
                                    let northLat = box.northLongitude.value || 90;
                                    let southLat = box.southLongitude.value || -90;
                                    let westLon = box.westLongitude.value || -180;
                                    const bounds = [parseFloat(southLat), parseFloat(northLat), parseFloat(westLon), parseFloat(eastLon)];
                                    let text = '['+bounds.toString()+']';
                                    textArray.push(text);
                                    let bbMetadata = {"Dataset" : item['name'], "Dataset ID":
                                            `<a target='_blank' rel='noreferrer' href='${displayItem.url}'>${item['global_id']}</a>`,
                                        "Dataverse": item['name_of_dataverse'], "BoundingBox": text};
                                    displayItem.coverage.forEach(item => { // extract locations to show them in the popup
                                        for (const [key, value] of Object.entries(item)) {
                                            let uppercase = key.charAt(0).toUpperCase() + key.slice(1);
                                            bbMetadata[uppercase] = value.value;
                                        }
                                    });
                                    const geojson = { "type": "FeatureCollection",
                                        "bbox": bounds,
                                        "features": [
                                            { "type": "Feature",
                                                "bbox": bounds,
                                                "geometry": {
                                                    "type": "Polygon",
                                                    "coordinates": [[
                                                        [eastLon, southLat], [westLon, southLat], [westLon, northLat], [eastLon, northLat],
                                                    ]],
                                                },
                                                "properties": bbMetadata,
                                            },
                                        ],
                                    };
                                    geojsonArray.push(geojson);
                                    let layerName = `${item.name} BBox`;
                                    if (status === 'DRAFT') layerName = `${status}: ${item.name} BBox`;
                                    if (searchSettings.automaticMapping) {
                                        dispatch(addGeoJSON(geojson, layerName, `${uniqueUpdateKey}_boundingBox`));
                                    }
                                });
                                displayItem.boundingBoxes = field.value; // for editing
                                displayItem.geojsonBoxes = geojsonArray; // for mapping
                                displayItem.item.BoundingBox = textArray.join(); // for text display
                            }
                        });
                        dispatch(updateResultItem({key: uniqueUpdateKey, property: 'coverage', value: displayItem.coverage}));
                        if (displayItem.boundingBoxes.length > 0) {
                            dispatch(updateResultItem({key: uniqueUpdateKey, property: 'boundingBoxes', value: displayItem.boundingBoxes}));
                            dispatch(updateResultItem({key: uniqueUpdateKey, property: 'geojsonBoxes', value: displayItem.geojsonBoxes}));
                        } else displayItem.item.BoundingBox = '(Not specified)';
                } else displayItem.item.BoundingBox = '(Not available: Geospatial metadata block not enabled)';
            });
        } else if (item.type === 'file')
            displayItem.key = String(item['file_id']);
        else displayItem.key = index;
        const dataItem = {};
        const displayFields = ['name', 'type', 'authors', 'majorVersion', 'versionState', 'global_id', 'citation', 'url', 'keywords', 'published_at',
            'name_of_dataverse', 'subjects', 'dataset_citation', 'dataset_name', 'file_type', 'file_id', 'file_content_type', 'size_in_bytes',
            'fileCount', 'boundingBoxes', 'geographicCoverage'];
        displayFields.forEach(k => {
            if (!item[k]) return;
            let v = item[k];
            let value = v;
            if (Array.isArray(v) && v.length > 1) value = v.join(', ');
            else if (Array.isArray(v) && v.length === 1) value = v[0];
            if (k === 'type') {
                value = v.charAt(0).toUpperCase() + v.slice(1);
                displayItem.type = value;
            } else if (k === 'url') {
                if (item.type === 'dataset'){
                    displayItem.url = process.env.REACT_APP_DATAVERSE_URL + '/dataset.xhtml?persistentId=' + encodeURIComponent(item['global_id']);
                } else displayItem.url = v;
                return;
            } else if (k === 'file_content_type') {
                const fileType = FileService.getFileType(v);
                displayItem.fileType = fileType;
                displayItem.restricted = true;
                let mappable = fileType !== UNSUPPORTED && fileType !== ZIPPED_FILE && fileType !== CSVFILE;
                if (mappable) displayItem.mapFile = mappable;
                displayItem.url = process.env.REACT_APP_DATAVERSE_URL + '/file.xhtml?fileId=' + item['file_id'];
                let metaDataURL = process.env.REACT_APP_DATAVERSE_URL + '/api/v1/files/' + item['file_id'] + '/metadata';
                const processFileMetaData = json => {
                    if (json.status === 'ERROR') return;
                    if (json['restricted'] === false) {
                        displayItem.restricted = false;
                        displayItem.downloadURL = item.url;
                        dispatch(updateResultItem({key: item['file_id'], property: 'restricted', value: false}));
                        dispatch(updateResultItem({key: item['file_id'], property: 'downloadURL', value: displayItem.downloadURL}));
                        /* if (json.categories)  {
                            dataItem['Tags'] = json.categories.join();
                            dispatch(updateResultItem({id: item['file_id'], property: 'Tags', value: json.categories.join()}));
                        } */
                        if (fileType === CSVFILE && json.categories) {
                            mappable = geoTagAvailable(json.categories);
                            if (!mappable && json['dataFileTags']) mappable = geoTagAvailable(json['dataFileTags']);
                            if (mappable) {
                                displayItem.mapFile = true;
                                dispatch(updateResultItem({key: item['file_id'], property: 'mapFile', value: mappable}));
                            }
                        }
                    } else { //permission is granted for the current API token
                        let fileUrl =  process.env.REACT_APP_DATAVERSE_URL + '/api/v1/access/datafile/' + item['file_id'] + '?gbrecs=true';
                        let headers = apiKey ? {'X-Dataverse-key': apiKey} : {};
                        fetch(fileUrl, {method: 'HEAD', headers: headers}).then(res => {
                            if (res.status === 200) {
                                displayItem.restricted = false;
                                dispatch(updateResultItem({key: item['file_id'], property: 'restricted', value: false}));
                            }
                        });
                    }
                };
                let headers = apiKey ? {'X-Dataverse-key': apiKey} : {};
                fetch(metaDataURL,
                    {method: 'GET', headers})
                    .then(res => res.json())
                    .then(json => {
                        if (Object.keys(json).length > 0) {
                            processFileMetaData(json);
                        } else {
                            fetch(metaDataURL+'/draft',
                                {method: 'GET', headers: headers})
                                .then(res => res.json())
                                .then(json => {
                                    if (Object.keys(json).length > 0) {
                                        processFileMetaData(json);
                                    }
                                });
                        }
                    });
                return;
            } else if (k === 'file_id') {
                displayItem.key = v;
                displayItem.fileID = v;
                return;
            } else if (k === 'geographicCoverage'){
                let coverageArray = [];
                v.forEach(item => {
                    let coverageString = [];
                    for (const value of Object.values(item))
                        coverageString.push(value);
                    coverageArray.push(coverageString.join(', '));
                });
                let formatted = coverageArray.join('');
                value = formatted.replaceAll(',,',',').trim();
                if (value[value.length - 1] === ',') value = value.slice(0, -1);
                if (value === '') return;
            } else if (k === 'majorVersion'){
                let minorVersion = item['minorVersion'] ? item['minorVersion'] : '0';
                value = `${v}.${minorVersion}`;
            } else if (k === 'size_in_bytes') {
                value = `${Number(v/(1024*1024)).toFixed(2)} MB`;
            } else if (k === 'published_at') value = new Date(v).toDateString();

            let key = formatSearchItem(k);
            dataItem[key] = value;
            displayItem.item = dataItem;
        });
        if (displayItem.item)
            currentResults.push(displayItem);
    });

    searchSettings.items = currentResults;
    return setResults(searchSettings);
};

const geoTagAvailable = (tags) => {
    let mappable = false;
    tags.forEach(tag => {
        if (tag.toLowerCase() === 'geospatial')
            mappable = true;
    });
    return mappable;
};


const SearchContainer = () => {

    const results = useSelector((state) => state.results);
    const apiKey = useSelector((state) => state.apiKey);
    const layout = useSelector((state) => state.layout);
    const bbox = useSelector((state) => state.bbox);
    const drawing = useSelector((state) => state.drawing);
    const searchEnvelope = useSelector((state) => state.searchEnvelope);
    const dispatch = useDispatch();

    const [searchTerm, setSearchTerm] = useState('');
    const [searchUpdateKey, setSearchUpdateKey] = useState('');
    const [resultsMessage, setResultsMessage] = useState({});
    const [pages, setNumberOfPages] = useState(1);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalCount, setTotalCount] = useState(0);
    const [filters, setFilters] = useState([]); // general type/tag filters
    const [countryFilters, setCountryFilters] = useState([]);
    const [dataverseFilters, setDataverseFilters]  = useState([]);
    const [dataverseSuggestions, setDataverseSuggestions] = useState([]);
    const [sortOrder, setSortOrder] = useState(null);
    const [sortOptions] = useState(getSortOptions);
    const [mappableFiles, setMappableFiles] = useState([]);
    const [otherFiles, setOtherFiles] = useState([]);
    const [viewFilesLabel, setViewFilesLabel] = useState('View Files');
    const [currentDataset, setCurrentDataset] = useState(null);
    const [currentFile, setCurrentFile] = useState(null);
    const [loadFileInfo, setLoadFileInfo] = useState({});
    const [loadErrors, setLoadErrors] = useState([]);
    const [checkedFiles, setCheckedFiles] = useState([]);
    const [moreFilterLabel, setMoreFilterLabel] = useState('More options');
    const [showMore, setShowMore] = useState(false);
    const [searchPageDisplayLabel, setSearchPageDisplay] = useState('Hide');
    const [automaticMapping, setAutomaticMapping] = useState(true);
    const [splitView, setSplitView] = useState(false);
    function usePrevious(value) {
        const ref = useRef();
        useEffect(() => {
            ref.current = value;
        });
        return ref.current;
    }
    const prevBBox = usePrevious(bbox);
    const prevEnvelope = usePrevious(searchEnvelope);
    useEffect(() => {
        if (prevBBox !== bbox){
            if (!bbox && results){
                    setResultsMessage({status: 'info', totalResults: results.totalCount, message: `Total results: ${results.totalCount}`,
                        subMessage: ``});
            }
        }
    }), [bbox];

    /** Starts a map search upon creation of a new search envelope. */
    useEffect(() => {
        if (prevEnvelope !== searchEnvelope){
            if (!searchEnvelope) return;
            reset(false); // keep sort order and filters
            const api = process.env.REACT_APP_SEARCH_API;
            const searchURL = `${api}/geoquery?envelope=${searchEnvelope}`;
            fetch(searchURL)
                .then(res => res.json())
                .then(json => {
                    if (json.response){
                        let found = json.response['numFound'];
                        let ids = [];
                        if (found > 0){
                            json.response['docs'].forEach(doc => {
                                ids.push(doc['id']);
                            });
                            let uniqueIDs = [...new Set(ids)];
                            const settings = {searchTerm: searchTerm, typeFilters: filters, dataverseFilters: dataverseFilters, countryFilters: countryFilters, totalCount: 0,
                                automaticMapping: automaticMapping, sortOrder: sortOrder, datasetFilters: uniqueIDs, offset: 0, currentPage: 1};
                            // if bounding boxes will be added to the map, switch to split view to leave the map visible
                            if (automaticMapping && !splitView) setSplitView(true);
                            loadFromDataverse(settings);
                        } else {
                            setTotalCount(0);
                            setResultsMessage({status: 'info', totalResults: 0, message: `Total results: 0`});
                        }
                    } else setResultsMessage({status: 'error', message: `An error occurred: (${json.error})`});
                }).catch((error) => {
                dispatch(stopLoading());
                setResultsMessage({status: 'error', message: `Could not reach API: (${error})`});
            });
        }
    }), [searchEnvelope];

    /** On tool mount. */
    useEffect(() => {
        getDataverseSuggestions(); // get list of available Dataverses
        if (results) { // ensure that previous results are still displayed after returning to search
            if (results.items && results.items.length > 0){
                setFilters(results.typeFilters);
                setCountryFilters(results.countryFilters);
                if (results.typeFilters.length > 0 || results.countryFilters.length > 0){
                    setShowMore(true);
                    setMoreFilterLabel('Less options');
                }
                setDataverseFilters(results.dataverseFilters);
                setAutomaticMapping(results.automaticMapping);
                setSortOrder(results.sortOrder);
                setSearchTerm(results.searchTerm);
                setCurrentPage(results.currentPage);
                setNumberOfPages(results.pages);
                setTotalCount(results.totalCount);
                if (results.totalCount > 0)
                    setResultsMessage({status: 'info', totalResults: results.totalCount, message: `Total results: ${results.totalCount}`});
           } else dispatch(clearResults());
        }
    }, []);

    /** Retrieves available Dataverse collections. */
    const getDataverseSuggestions = () => {
        let newSuggestions = [{
            value: 'Error',
            label: 'Suggestions could not be loaded!',
        }];
        fetch(process.env.REACT_APP_DATAVERSE_URL +'/api/v1/search?q=*&start=0&type=dataverse&per_page=1000&sort=name&order=asc')
            .then(res => res.json())
            .then(json => {
                if (json.data && json.data.items) {
                    newSuggestions = json.data.items.map(dataverse => {
                        return {
                            value: dataverse.identifier,
                            label: dataverse.name,
                        };
                    });
                }
                setDataverseSuggestions(newSuggestions);
            }).catch(() => {
            setDataverseSuggestions(newSuggestions);
        });
    };

    const toggleSplitView = () => {
        if (splitView) setSplitView(false);
        else setSplitView(true);
    };

    const changePage = (event, value) => {
        setCurrentPage(value);
        switchToPage(value);
    };

    const toggleSearchPageDisplay = () => {
        if (searchPageDisplayLabel === 'Hide'){
            setSearchPageDisplay('Show');
        } else setSearchPageDisplay('Hide');
    };

    const toggleMoreFilters = () => {
        const toggleValue = !showMore;
        if (toggleValue) setMoreFilterLabel('Less options');
        else setMoreFilterLabel('More options');
        return setShowMore(toggleValue);
    };

    const switchToPage = (page) => {
        const newOffset = (page - 1) * 10;
        const settings = {searchTerm: searchTerm, typeFilters: filters, countryFilters: countryFilters, datasetFilters: [], dataverseFilters: dataverseFilters,
            totalCount: totalCount, offset: newOffset, sortOrder: sortOrder, pages: pages, automaticMapping: automaticMapping, currentPage: page};
       loadFromDataverse(settings);
    };

    /** Starts a search based on a search term and current settings. */
    const search = (searchTerm) => {
        setCurrentPage(1);
        setResultsMessage({status: 'info', message: 'Searching...'});
        const settings = {searchTerm: searchTerm, typeFilters: filters, countryFilters: countryFilters, datasetFilters: [], dataverseFilters: dataverseFilters,
            totalCount: totalCount, offset: 0, sortOrder: sortOrder, pages: pages, automaticMapping: automaticMapping, currentPage: 1};
        loadFromDataverse(settings);
    };

    /**
     * Retrieves and lists search results based on given settings.
     * */
    const loadFromDataverse = (settings) => {
        Map.removeBoundingBoxLayers();
        searchDataverse(settings, setResultsMessage, apiKey).then(updatedSettings => {
                if (updatedSettings.items.length === 0) return dispatch(clearResults());
                setNumberOfPages(updatedSettings.pages);
                setTotalCount(updatedSettings.totalCount);
                return dispatch(listResults(updatedSettings, apiKey, dispatch));
        });
    };

    const handleFileSelection = (selectedFiles) => {
        setCheckedFiles(selectedFiles);
    };

    const selectFilesToMap = () => {
        setLoadFileInfo({status: 'INFO', message: ''});
        const selectedFiles = mappableFiles.filter(file => {return checkedFiles.includes(file['fileID'])});
        if (selectedFiles.length > 0) {
            batchFileMapping(selectedFiles, apiKey, addVectorData, addRaster, dispatch).then(errorLog => {
                dispatch(stopLoading());
                if (errorLog.length > 0) {
                    setLoadFileInfo({status: 'WARNING', message: 'The following files have been skipped: '});
                    setLoadErrors(errorLog);
                } else {
                    if (!splitView) toggleSearchPageDisplay();
                    setLoadErrors([]);
                }
            });
        } else setLoadFileInfo({status: 'WARNING', message: 'No files selected!'});
    };

    const handleFilters = (filters) => {
        return setFilters(filters);
    };
    const handleCountryFilters = (countryFilters) => {
        return setCountryFilters(countryFilters);
    };
    const handleDataverseFilters = (dataverseFilters) => {
        setDataverseFilters(dataverseFilters);
    };
    const handleAutomaticMapping = (event, checked) => {
        setAutomaticMapping(checked);
    };
    const handleSorting = (item) => {
        let sortBy = item;
        if (item) sortBy = item.value;
        return setSortOrder(sortBy);
    };
    const changeViewFilesLabel = (label) => {
        return setViewFilesLabel(label);
    };
    const setCurrentFileID = (id) => {
        return setCurrentFile(id);
    };
    const setCurrentDatasetID = (id) => {
        return setCurrentDataset(id);
    };
    const updateSearchTerm = event => {
        if (event.target.value === '')
            dispatch(updateResults({key: 'searchTerm', value: ''}));
        return setSearchTerm(event.target.value);
    };
    const openItemLink = url => {
        window.open(url, "_blank");
    };
    const openItemMap = (url, fileName, fileID, contentType) => {
        return mapFileToMap(url, fileName, fileID, contentType, apiKey, addVectorData, addRaster, setLoadFileInfo, dispatch, splitView, toggleSearchPageDisplay);
    };

    /**
     * Loads files belonging to a dataset and analyses their mapping and access properties.
     * */
    const loadDatasetFiles = (datasetID, versionState) => {
        resetFiles();
        setLoadFileInfo({status: 'INFO', message: 'Loading...'});
        setCheckedFiles([]);
        setLoadErrors([]);
        let supportedFiles = [];
        let otherFiles = [];
        let errorMessage;
        let versionPath;
        if (versionState === 'DRAFT') {
            versionPath = 'versions/:draft';
        } else versionPath = 'versions/:latest-published';
        const filesDirURL = process.env.REACT_APP_DATAVERSE_URL+'/api/v1/datasets/:persistentId/'+versionPath+'?persistentId='+
            encodeURIComponent(datasetID);
        let headers = apiKey ? {'X-Dataverse-key': apiKey} : {};
        fetch(filesDirURL, {method: 'GET', headers: headers})
            .then(res => res.json())
            .then(json => {
                if (json.status === 'OK' && json.data){
                    const files = json.data.files ? json.data.files : [];
                    files.forEach(file => {
                        const fileType = FileService.getFileType(file['dataFile'].contentType);
                        const tags = file.categories ? file.categories : [];
                        let mappable = fileType !== UNSUPPORTED && fileType !== ZIPPED_FILE;
                        if (fileType === CSVFILE) {
                            mappable = geoTagAvailable(tags);
                            if (!mappable && file['dataFile']['tabularTags'])
                                mappable = geoTagAvailable(file['dataFile']['tabularTags']);
                        }
                        const item = file['dataFile'];
                        const fileURL = process.env.REACT_APP_DATAVERSE_URL+'/api/v1/access/datafile/'+item.id;
                        const pageURL = process.env.REACT_APP_DATAVERSE_URL+'/file.xhtml?fileId='+item.id;
                        const fileSize = `${Number(item['filesize']/(1024*1024)).toFixed(2)}`;
                        const fileObj = {restricted: file['restricted'], id: item.id, datasetID: datasetID, pageURL: pageURL, url: fileURL, mappable: mappable,
                            fileName: item['filename'], fileID: item.id, fileSize: fileSize, contentType: fileType};
                        if (mappable) supportedFiles.push(fileObj);
                        else otherFiles.push({fileName: item['filename'], pageURL: pageURL});
                    });
                    setLoadFileInfo({status: 'INFO', message: ''});
                } else {
                    errorMessage = json.message;
                    if (errorMessage) {
                        if (errorMessage.trim() === 'Bad api key') errorMessage = '(Unauthorized)';
                        setLoadFileInfo({status: 'WARNING', message: 'Files not accessible! ' + errorMessage});
                    } else setLoadFileInfo({status: 'WARNING', message: 'Files not accessible!'});
                }
                setMappableFiles(supportedFiles);
                setOtherFiles(otherFiles);
            }).catch((error) => {
                resetFiles();
                setLoadFileInfo({status: 'ERROR', message: `An error occurred: (${error})`});
        });

    };

    const submitBoundingBox = (datasetID) => {
        let boxes = Map.saveBoundingBox(false);
        dispatch(updateBoundingBox(boxes));
        dispatch(stopDrawing());
        ['_DRAFT', '_RELEASED'].forEach(suffix => {
            datasetID = datasetID.split(suffix)[0];
        });
        const json = {
            "fields": [
                {
                    "typeName": "geographicBoundingBox", "multiple": true, "typeClass": "compound",
                    "value": boxes,
                }],
        };
        const editURL =  process.env.REACT_APP_DATAVERSE_URL + '/api/v1/datasets/:persistentId/editMetadata?persistentId=' +
            encodeURIComponent(datasetID) + '&replace=true';
        let headers = apiKey ? {'X-Dataverse-key': apiKey, 'Content-Type': 'application/json'} : {};
        dispatch(startLoading(` Updating draft version...`));
        fetch(editURL, {method: 'PUT', headers: headers, body: JSON.stringify(json)})
            .then(res => res.json())
            .then(json => {
            toggleSearchPageDisplay();
            dispatch(removeBoundingBox());
            if (json.status === 'OK'){
                reset(true); // make sure that Dataset can be displayed by resetting filters
                setResultsMessage({status: 'info', message: 'Loading updated dataset...'});
                const settings = {searchTerm: '', typeFilters: [], countryFilters: [], totalCount: 0,
                    datasetFilters: [datasetID], offset: 0, currentPage: 1};
                loadFromDataverse(settings);
            } else setResultsMessage({status: 'error', message: `Error: ${json.message}`});
            dispatch(stopLoading());
        }).catch(error => {
            setResultsMessage({status: 'error', message: `Error: ${error}`});
        });

    };

    const setCurrentBoundingBox = rectangles => {
        return dispatch(updateBoundingBox(rectangles));
    };

    const boxesToRectangles = boxes => {
        let rectangles = [];
        boxes.forEach(box => {
            let eastLon = box.eastLongitude.value || 180;
            let northLat = box.northLongitude.value || 90;
            let southLat = box.southLongitude.value || -90;
            let westLon = box.westLongitude.value || -180;
            rectangles.push([[northLat, eastLon], [southLat, westLon]]);
        });
        return rectangles;
    };

    const resetBoundingBoxes = (originalBoxes) => {
        if (originalBoxes) {
            let rectangles = boxesToRectangles(originalBoxes);
            dispatch(startDrawing({mode: 'editing', currentBoxes: rectangles}));
        } else {
            dispatch(stopDrawing());
            toggleSearchPageDisplay();
            setResultsMessage({status: 'info', totalResults: results.totalCount, message: `Total results: ${results.totalCount}`,
                subMessage: ``});
        }
    };

    const drawBoundingBox = (currentBoxes) => {
        toggleSearchPageDisplay();
        let rectangles = boxesToRectangles(currentBoxes);
        if (!currentBoxes) dispatch(startDrawing({mode: 'drawing'}));
        else dispatch(startDrawing({mode: 'editing', currentBoxes: rectangles}));
        setResultsMessage({status: 'info', totalResults: results.totalCount, message: `Total results: ${results.totalCount}`,
            subMessage: `Please use the editing tools on the map to add, modify or delete bounding boxes. 
            Note that it is not possible to completely delete all bounding boxes of a Dataset via GeoMapper
             and that all changes will be saved automatically when clicking on 'Submit'.`});
    };

    const editDataset = (action, persistentID, datasetID) => {
            const headers = apiKey ? {'X-Dataverse-key': apiKey, 'Content-Type': 'application/json'} : {};
            let actionURL = process.env.REACT_APP_DATAVERSE_URL +
                '/api/v1/datasets/:persistentId/actions/:publish?persistentId=' + encodeURIComponent(persistentID) + '&type=minor';
            let method = 'POST';
            if (action === 'delete'){
                actionURL =  process.env.REACT_APP_DATAVERSE_URL + '/api/v1/datasets/' + datasetID + '/versions/:draft';
                dispatch(startLoading(`Deleting draft...`));
                dispatch(removeBoundingBox());
                method = 'DELETE';
            } else if (action === 'publish'){
                dispatch(startLoading(`Publishing new version...`));
            }
            fetch(actionURL, {method: method, headers: headers})
                .then(res => res.json())
                .then(json => {
                    if (json.status === 'OK'){
                        reset(true); // remove filters
                        setResultsMessage({status: 'info', message: 'Loading updated dataset...'});
                        const settings = {searchTerm: '', typeFilters: [], countryFilters: [], totalCount: 0,
                            datasetFilters: [persistentID], offset: 0, automaticMapping: automaticMapping, currentPage: 1};
                        loadFromDataverse(settings);
                        dispatch(stopLoading());
                    } else setResultsMessage({status: 'error', message: `Error: ${json.message}`});
                }).catch(error => {
                setResultsMessage({status: 'error', message: `Error: ${error}`});
            });

    };

    const mapBoundingBox = (layerName, layerId, boxes) => {
        boxes.forEach(geojsonBox => {
            dispatch(addGeoJSON(geojsonBox, layerName, layerId));
        });
    };
    const resetFiles = () => {
        setMappableFiles([]);
        setOtherFiles([]);
    };

    const reset = (clearFilters) => {
        if (clearFilters){
            setSearchUpdateKey(Date.now());
            setSearchTerm('');
            setFilters([]);
            setCountryFilters([]);
            setSortOrder(null);
            setDataverseFilters([]);
        }
        dispatch(clearResults());
        setResultsMessage({});
        setLoadFileInfo({status: 'INFO', message: ''});
        setCurrentDataset(null);
        setCurrentFile(null);
        setMappableFiles([]);
        setCheckedFiles([]);
        dispatch(removeBoundingBox());
        dispatch(removeSearchEnvelope());
        Map.removeBoundingBoxLayers();
        dispatch(stopDrawing());
    };
    return SearchComponent(
        {
            results,
            updateSearchTerm,
            search,
            searchTerm,
            reset,
            searchUpdateKey,
            resultsMessage,
            openItemLink,
            openItemMap,
            loadDatasetFiles,
            currentPage,
            pages,
            changePage,
            handleFilters,
            filters,
            handleCountryFilters,
            countryFilters,
            sortOptions,
            sortOrder,
            handleSorting,
            mappableFiles,
            otherFiles,
            viewFilesLabel,
            changeViewFilesLabel,
            currentDataset,
            setCurrentDatasetID,
            currentFile,
            setCurrentFileID,
            loadFileInfo,
            loadErrors,
            handleFileSelection,
            selectFilesToMap,
            mapBoundingBox,
            toggleMoreFilters,
            moreFilterLabel,
            showMore,
            layout,
            searchPageDisplayLabel,
            toggleSearchPageDisplay,
            editDataset,
            bbox,
            submitBoundingBox,
            setCurrentBoundingBox,
            drawBoundingBox,
            drawing,
            resetBoundingBoxes,
            apiKey,
            automaticMapping,
            handleAutomaticMapping,
            splitView,
            toggleSplitView,
            dataverseFilters,
            handleDataverseFilters,
            dataverseSuggestions,
        },
    );
};

export default SearchContainer;
