import React from 'react';
import ToolHeader from '../../tool-elements/tool-header';
import ToolContent from '../../tool-elements/tool-content';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Pagination from '@material-ui/lab/Pagination';
import {
    Box,
    Checkbox,
    FormControlLabel,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
} from '@material-ui/core';
import Select from 'react-select';
import ToolFooter from "../../tool-elements/tool-footer";
import {DataGrid} from '@mui/x-data-grid';
import countryList from "../../../constants/countries.json";
import {customSelectStyle} from "../../tool-elements/tool-content/tool-content.component";
import arrow from "../../../assets/images/arrow_back_black.svg";

const mapAsLinkList = (file, index) => {
    return (<li key={index} className={'search-results-text'}>
        <a target='_blank' className={'file-link'}
           href={file['pageURL']} rel="noreferrer">{file['fileName']}</a>
    </li>);
};

const mapErrors = (file, index) => {
    return (<li key={index} className={'search-results-text'}>{file}</li>);
};

const renderMessage = (option) => {
    switch (option.status) {
        case 'INFO':
            return (<b className='smaller-text'>{option.message}</b>);
        case 'ERROR':
            return (<b className='smallest-text error-message'>{option.message}</b>);
        case 'WARNING':
            return (<b className='smallest-text warning'>{option.message} </b>);
        default:
            return (<span> {option.message} </span>);
    }
};
const mapSearchResults = (result, openItemLink, openItemMap, loadDatasetFiles, mappableFiles, otherFiles, viewFilesLabel,
                          changeViewFilesLabel, currentDataset, setCurrentDatasetID, currentFile, setCurrentFileID,
                          loadFileInfo, loadErrors, handleFileSelection, selectFilesToMap, mapBoundingBox,
                          toggleSearchPageDisplay, editDataset, setCurrentBoundingBox, drawBoundingBox, apiKey) => {

    return (
        <Card key={'search_item_' + result.key} className='search-card' variant="outlined">
            <CardContent>
                <TableContainer> <Table>
                    <TableBody>
                        {
                            Object.entries(result.item).map(([key, value], index) => {
                                return (
                                    <TableRow className={index % 2 ? 'row-background-odd' : 'row-background-even'}
                                              key={key}>
                                        <TableCell width={'100px'}>
                                            <b>{key}</b>
                                        </TableCell>
                                        <TableCell component="th" scope="row" align="inherit"
                                                   className='search-results-text'
                                        >
                                            {value.toString()}
                                        </TableCell>
                                    </TableRow>
                                );
                            })
                        }
                    </TableBody></Table></TableContainer>
                {otherFiles.length > 0 && result.key === currentDataset &&
                <div className={'small-padding smaller-text'}
                     style={viewFilesLabel === 'Hide Files' ? {display: 'block'} : {display: 'none'}}>
                    <b>Non-mappable files:</b>
                    <div className={'small-scrollable'}>
                        <ul>{otherFiles.map(mapAsLinkList)}</ul>
                    </div>
                    {mappableFiles.length > 0 &&
                    <b className={'smaller-text'}>Files with mappable file types:</b>}
                </div>}
                {mappableFiles.length > 0 && result.key === currentDataset &&
                <div className={'padding-top'}
                     style={viewFilesLabel === 'Hide Files' ? {display: 'block'} : {display: 'none'}}>
                    <div className={'search-files-list blue-scrollbar'}>
                        <DataGrid
                            rows={mappableFiles}
                            columns={[
                                {
                                    field: 'fileSize',
                                    headerName: 'MB',
                                    headerAlign: 'center',
                                    type: 'number',
                                    valueFormatter: (params) => {
                                        const valueFormatted = Number(params.value);
                                        return `${valueFormatted}`;
                                    },
                                },
                                {
                                    field: 'fileName',
                                    headerName: 'Name',
                                    headerAlign: 'center',
                                    width: 300,
                                    renderCell: function createLink(params) {
                                        return (
                                            <a target='_blank' style={{
                                                textOverflow: 'ellipsis',
                                                overflow: 'hidden',
                                                color: 'inherit',
                                                display: 'block',
                                            }}
                                               href={params.row.pageURL}
                                               rel="noreferrer">{params.value}</a>);
                                    },
                                },
                                {
                                    field: 'restricted',
                                    headerName: 'Restricted Access',
                                    headerAlign: 'center',
                                    type: 'boolean',
                                    width: 200,
                                },
                            ]}
                            onSelectionModelChange={(newSelectionModel) => {
                                handleFileSelection(newSelectionModel);
                            }}
                            isRowSelectable={(params) => !(params.row.restricted === true && !apiKey)}
                            checkboxSelection={true}
                            hideFooterPagination={true}
                            hideFooter={true}
                            disableColumnMenu={true}
                            rowHeight={30}
                            headerHeight={50}
                        />
                    </div>
                </div>
                }

                {((currentDataset === result.key && viewFilesLabel === 'Hide Files')
                    || (result.mapFile && currentFile === result.fileID)) &&
                <div> {renderMessage(loadFileInfo)}
                    {loadErrors.length > 0 && <div className={'small-scrollable'}>
                        <ul>{loadErrors.map(mapErrors)}</ul>
                    </div>
                    }
                </div>}

                {mappableFiles.length > 0 && result.key === currentDataset &&
                <Button style={viewFilesLabel === 'Hide Files' ? {
                    display: 'block',
                    paddingTop: 10,
                    paddingBottom: 0,
                } : {display: 'none'}}
                        color='primary' size="small" onClick={() => {
                    selectFilesToMap();
                }}>Map selected files</Button>}

            </CardContent>

            <CardActions>
                {<Button title="Open in new tab" onClick={() => {
                    openItemLink(result.url);
                }} color='primary' size="small"><b>Open in {process.env.REACT_APP_DATAVERSE}</b></Button>}
                {result.type === 'File' && <div title={result.restricted ? 'Not available (no permission)' : ''}>
                    <Button title="Download this file" disabled={result.restricted} onClick={() => {
                        openItemLink(result.downloadURL);
                    }} color='primary' size="small"><b>Download</b></Button></div>}
                {result.type === 'Dataset' && parseInt(result.item.Files) > 0 &&
                <Button title='List files in this dataset' disabled={result.restricted}
                        onClick={() => {
                            if (currentDataset === result.key && viewFilesLabel === 'Hide Files')
                                changeViewFilesLabel('View Files');
                            else { //new dataset selected
                                setCurrentDatasetID(result.key);
                                loadDatasetFiles(result.item.ID, result.item['Status']);
                                changeViewFilesLabel('Hide Files');
                            }
                        }} color='primary'
                        size="small"><b>{currentDataset === result.key ? viewFilesLabel : 'View Files'}</b>
                </Button>}
                {result.mapFile && <div title={result.restricted ? 'Not available (no permission)' : ''}>
                    <Button title="Load and map this file" disabled={result.restricted} onClick={() => {
                        setCurrentFileID(result.fileID);
                        openItemMap(result.url, result.item.Name, result.fileID, result.fileType);
                    }} color='primary' size="small"><b>Add to Map</b>
                    </Button></div>}
                {result.type === 'Dataset' && result.geospatialMetadataEnabled &&
                <div
                    title={!(result.boundingBoxes && result.boundingBoxes.length > 0) ? 'Function not available (no bounding boxes)' : ''}>
                    <Button disabled={!(result.boundingBoxes && result.boundingBoxes.length > 0)}
                            title="View geographic context of this dataset"
                            onClick={() => {
                                if (currentDataset !== result.key)
                                    setCurrentDatasetID(result.key);
                                toggleSearchPageDisplay();
                                const layerName = `${result.item.Name} BB (${result.item.Status})`;
                                const layerId = `${result.key}_boundingBox`;
                                mapBoundingBox(layerName, layerId, result.geojsonBoxes);
                            }} color='primary' size="small"><b>Map Bounding Box</b>
                    </Button>
                </div>
                }
                {result.type === 'Dataset' && result.geospatialMetadataEnabled &&
                <div title={(result.restricted || !result.editable) ? 'Function not available (no permission)' : ''}>
                    <Button color='primary' size="small"
                            disabled={result.restricted || !result.editable}
                            title="Add/update bounding box" onClick={() => {
                        if (currentDataset !== result.key)
                            setCurrentDatasetID(result.key);
                        if (result.boundingBoxes)
                            setCurrentBoundingBox(result.boundingBoxes);
                        drawBoundingBox(result.boundingBoxes);
                    }}
                    ><b>Edit Bounding Box</b></Button>
                </div>
                }
                {result.type === 'Dataset' && result.isDraft && result.datasetId &&
                <div title={(result.restricted || !result.editable) ? 'Function not available (no permission)' : ''}>
                    <Button color='primary' size="small"
                            disabled={result.restricted || !result.editable}
                            title="Publish new minor version based on this draft" onClick={() => {
                        if (currentDataset !== result.key)
                            setCurrentDatasetID(result.key);
                        editDataset('publish', result.item.ID, result.datasetId);
                    }}
                    ><b>Publish</b>
                    </Button>
                </div>

                }
                {result.type === 'Dataset' && result.isDraft && result.datasetId &&
                <div title={(result.restricted || !result.editable) ? 'Function not available (no permission)' : ''}>
                    <Button color='primary' size="small"
                            disabled={result.restricted || !result.editable}
                            title="Delete Draft" onClick={() => {
                        if (currentDataset !== result.key)
                            setCurrentDatasetID(result.key);
                        editDataset('delete', result.item.ID, result.datasetId);
                    }}
                    ><b>Delete</b>
                    </Button>
                </div>

                }
            </CardActions>
        </Card>
    );
};


const SearchComponent = ({
                                 results,
                                 updateSearchTerm,
                                 searchTerm,
                                 search,
                                 reset,
                                 searchUpdateKey,
                                 resultsMessage,
                                 openItemLink,
                                 openItemMap,
                                 loadDatasetFiles,
                                 currentPage,
                                 pages,
                                 changePage,
                                 handleFilters,
                                 filters,
                                 handleCountryFilters,
                                 countryFilters,
                                 sortOptions,
                                 sortOrder,
                                 handleSorting,
                                 mappableFiles,
                                 otherFiles,
                                 viewFilesLabel,
                                 changeViewFilesLabel,
                                 currentDataset,
                                 setCurrentDatasetID,
                                 currentFile,
                                 setCurrentFileID,
                                 loadFileInfo,
                                 loadErrors,
                                 handleFileSelection,
                                 selectFilesToMap,
                                 mapBoundingBox,
                                 toggleMoreFilters,
                                 moreFilterLabel,
                                 showMore,
                                 layout,
                                 searchPageDisplayLabel,
                                 toggleSearchPageDisplay,
                                 editDataset,
                                 bbox,
                                 submitBoundingBox,
                                 setCurrentBoundingBox,
                                 drawBoundingBox,
                                 drawing,
                                 resetBoundingBoxes,
                                 apiKey,
                                 automaticMapping,
                                 handleAutomaticMapping,
                                 splitView,
                                 toggleSplitView,
                                 dataverseFilters,
                                 handleDataverseFilters,
                                 dataverseSuggestions,
                             }) => (

    <div>
        <div id='search-tool' className={layout === 'hide-menu' ? 'tool-hidden' : 'tool no-scroll'}>
            <ToolHeader
                iconClass="search-tool-icon-white"
                title={'Explore ' + process.env.REACT_APP_DATAVERSE}
            />

            <div className="search-input tool-spacing-top">
                <input type="search"
                       autoFocus
                       key={searchUpdateKey}
                       placeholder={`Search ${process.env.REACT_APP_DATAVERSE} records...`}
                       onChange={updateSearchTerm}
                       value={searchTerm}
                       onKeyDown={(e) => {
                           if (e.key === 'Enter') {
                               search(searchTerm);
                           }
                       }}
                />
                <input type="button" onClick={() => {
                    if (searchPageDisplayLabel === 'Show') toggleSearchPageDisplay();
                    search(searchTerm);
                }
                }/>
                <br/><br/>

                <Select
                    className={'small-spacing sort-options no-center'}
                    onChange={handleSorting}
                    placeholder={'Sort results by...'}
                    options={sortOptions}
                    value={sortOptions.filter(option => {
                        return option.value === sortOrder;
                    })}
                    isClearable='true'
                />

                <Select
                    className={'small-spacing clear-float filter-options no-center'}
                    options={[
                        {label: "Files", value: "files"},
                        {label: "Datasets", value: "datasets"},
                        {label: "Dataverses", value: "dataverses"},
                        {label: "Geospatial Tags", value: "geospatial"},
                        {label: "Geospatial Tabular Data", value: "geospatial_csv"},
                    ]}
                    isMulti
                    value={filters}
                    placeholder={'Filter by type or tag...'}
                    onChange={handleFilters}
                />

                <Select styles={customSelectStyle}
                        className={'small-spacing clear-float dataverse-options no-center'}
                        onChange={handleDataverseFilters}
                        placeholder={'Filter by Dataverse collection...'}
                        options={dataverseSuggestions}
                        isMulti
                        value={dataverseFilters}
                />

                {showMore &&
                <div>
                    <Select styles={customSelectStyle}
                            className={'small-spacing clear-float country-options no-center'}
                            onChange={handleCountryFilters}
                            placeholder={'Filter by country...'}
                            options={countryList.countries}
                            isMulti
                            value={countryFilters}
                    />

                    <div className={'no-center'}>
                        <FormControlLabel
                            title={"Choose whether Dataset bounding boxes should be added to the map automatically for each search results page."}
                            control={<Checkbox checked={automaticMapping}
                                               size="small"
                                               onChange={handleAutomaticMapping}
                                               inputProps={{'aria-label': 'controlled'}}
                            />} label={<Box component="div" fontSize={15}>Add bounding boxes per result page</Box>}/>
                    </div>
                </div>
                }

                <div className='smaller-spacing'>
                    <button className={'underline-button'} onClick={toggleMoreFilters}>
                        {moreFilterLabel}
                    </button>
                    <button className={'underline-button'} onClick={() => {
                        reset(true);
                    }}>
                        Clear Search
                    </button>
                    <button className={'underline-button'} onClick={() => {
                        search(searchTerm);
                    }}>
                        Reload
                    </button>
                    <br/>
                    {resultsMessage !== {} && resultsMessage.message &&
                    <div className={'no-center'}>
                        <span className={`results-message smaller-text' ${resultsMessage.status}-message`}>
                            {resultsMessage.message}
                        </span>
                        {resultsMessage.totalResults > 0 &&
                        <button className={'underline-button no-float'}
                                onClick={toggleSearchPageDisplay}>{searchPageDisplayLabel}
                        </button>}
                    </div>}
                    {resultsMessage.subMessage &&
                    <i className={`results-message smaller-text' ${resultsMessage.status}-message`}>{resultsMessage.subMessage}</i>
                    }

                    {results && bbox && drawing &&
                    <div className={'inline-flex'}>
                        <div className='small-margin'>
                            <button
                                title={'Update bounding boxes'}
                                className='gt-button-small'
                                onClick={() => {
                                    submitBoundingBox(currentDataset);
                                }}
                            >
                                Submit
                            </button>
                        </div>
                        <div className='small-margin'>
                            <button
                                className='gt-button-small'
                                title={'Reset your changes'}
                                onClick={() => {
                                    let currentItem = results.items.filter(option => {
                                        return option.key === currentDataset;
                                    });
                                    if (currentItem.length > 0) {
                                        currentItem = currentItem[0];
                                        setCurrentBoundingBox(currentItem.boundingBoxes);
                                        resetBoundingBoxes(currentItem.boundingBoxes);
                                    }
                                }}
                            >
                                Reset
                            </button>
                        </div>
                        <div className='small-margin'>
                            <button
                                className='gt-button-small'
                                title={'Stop editing'}
                                onClick={() => {
                                    setCurrentBoundingBox(null);
                                    resetBoundingBoxes(null);
                                }}
                            >
                                Cancel
                            </button>
                        </div>
                    </div>
                    }

                </div>

            </div>
            <ToolFooter/>

        </div>

        {results && results.items && searchPageDisplayLabel === 'Hide' &&
        <div id={'search-results'}>
            <div className={layout === 'hide-menu' ? 'hidden' : ''}>
                <div className={splitView ? 'search-list-container splitView' : 'search-list-container'}>
                <div className={layout === 'hide-menu' ? 'hidden' : 'search-list-header tool-spacing-sides'}>
                    <div className={'header-items'}>
                        <img onClick={() => {
                            toggleSearchPageDisplay();
                        }} title="Back to Map View" alt="Back" src={arrow}/>
                        <h3>Search results: {resultsMessage.totalResults}</h3>
                        <button className={splitView ? 'collapse-button-open right' : 'collapse-button right'} onClick={() => {
                            toggleSplitView();
                        }} title={splitView ? 'Expand' : 'Collapse'}/>
                    </div>
                </div>
                <ToolContent className={`search-list tool-spacing-sides clear-float`}>
                    {results.items.map(function (x) {
                        return mapSearchResults(x, openItemLink, openItemMap, loadDatasetFiles, mappableFiles, otherFiles, viewFilesLabel,
                            changeViewFilesLabel, currentDataset, setCurrentDatasetID, currentFile, setCurrentFileID, loadFileInfo, loadErrors,
                            handleFileSelection, selectFilesToMap, mapBoundingBox, toggleSearchPageDisplay, editDataset, setCurrentBoundingBox,
                            drawBoundingBox, apiKey);
                    })}
                </ToolContent>
                </div>
            </div>
            <div
                className={layout === 'hide-menu' ? 'hidden' : 'tool-spacing-top-bottom-small auto-margin pagination-list'}>
                {results &&
                <Pagination color='primary' defaultPage={1} page={currentPage}
                            count={pages} onChange={changePage}/>}
            </div>
        </div>
        }

    </div>
);

export default SearchComponent;
