import LoaderComponent from './loader.component';
import {useSelector} from 'react-redux';

const LoaderContainer = () => {
    const loading = useSelector((state) => state.loading);
    return LoaderComponent({loading});
};

export default LoaderContainer;
