import {useDispatch} from 'react-redux';
import ToolHeaderComponent from './tool-header.component';
import {focusMenu} from '../../../actions/menu-focus-actions';

const ToolHeaderContainer = ({iconClass, title}) => {
    const dispatch = useDispatch();
    const focus = () => {
        return dispatch(focusMenu());
    };
    return ToolHeaderComponent({iconClass, title, focus});
};

export default ToolHeaderContainer;
