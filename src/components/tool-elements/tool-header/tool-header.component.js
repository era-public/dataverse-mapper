import React from 'react';
import {Link} from 'react-router-dom';
import arrow from '../../../assets/images/arrow_back.svg';
const ToolHeaderComponent = ({iconClass, title, focus}) => (
    <header onClick={focus}>
        <div className="header-top-row">
            <Link to="/" >
                <img className="back-arrow-icon" alt="Back" src={arrow}/>
                <span className="small-padding">Home</span>
            </Link>
        </div>
        <div className="header-title-row">
            <span className={'tool-icon '+ iconClass}/>
            <p>{title}</p>
        </div>
    </header>
);

export default ToolHeaderComponent;
