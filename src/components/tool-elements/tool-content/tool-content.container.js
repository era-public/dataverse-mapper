import {useSelector} from 'react-redux';
import ToolContentComponent from './tool-content.component';

const ToolContentContainer = ({children, className}) => {
    const menuFocus = useSelector((state) => state.menuFocus);
    return ToolContentComponent({children, menuFocus, className});
};

export default ToolContentContainer;
