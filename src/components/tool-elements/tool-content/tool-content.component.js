import React from 'react';
const customScrollBar = (base) => {
    return {
        ...base,
        "scrollbarWidth": "thin",
        "::-webkit-scrollbar": {
            width: "4px",
            height: "4px",
        },
        "::-webkit-scrollbar-track": {
            background: "rgba(49, 185, 141, .3)",
        },
        "::-webkit-scrollbar-thumb": {
            background: "rgba(49, 185, 141, 1)",
        },
    };
};
export const customSelectStyle = {
    menu: (base) => (customScrollBar(base)),
    menuList: (base) => {
        const customStyle = customScrollBar(base);
        customStyle.maxHeight = '250px';
        customStyle.overflowY = 'auto';
        return customStyle;
    },
};
export const smallCustomSelectStyle = {
    menu: (base) => (customScrollBar(base)),
    menuList: (base) => {
        const customStyle = customScrollBar(base);
        customStyle.maxHeight = '150px';
        customStyle.overflowY = 'auto';
        return customStyle;
    },
};
const ToolContentComponent = ({children, menuFocus, className}) => (
    <section className={`content ${menuFocus ? 'focus' : ''} ${className}`}>
        {children}
    </section>
);

export default ToolContentComponent;
