import React from 'react';
import {Link} from 'react-router-dom';

const ToolButtonComponent = ({name, iconClass, path, tooltip}) => (
    <Link to={path} className='tool-button' title={tooltip}>
        <i className = {iconClass}/>
        <p>{name}</p>
    </Link>
);

export default ToolButtonComponent;
