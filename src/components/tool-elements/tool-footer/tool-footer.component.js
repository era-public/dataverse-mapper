import React from 'react';

const ToolFooterComponent = () => (
    <footer className='tool-footer'>
        <i></i>
        <h3>{process.env.REACT_APP_TITLE}</h3>
    </footer>
);

export default ToolFooterComponent;
