import AppComponent from './app.component';
import {withRouter} from 'react-router-dom';
import {useSelector} from "react-redux";


const AppContainer = () => {
    const layout = useSelector((state) => state.layout);
    const searchEnvelope = useSelector((state) => state.searchEnvelope);
    return AppComponent({layout, searchEnvelope});
};

export default withRouter(AppContainer);
