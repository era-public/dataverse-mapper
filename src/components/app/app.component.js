import React from 'react';
import {Route, Switch, Redirect} from 'react-router-dom';
import MapContainer from '../map-container';
import Menu from '../menu';
import NavbarContainer from "../navbar/navbar.container";
import Load from '../tools/load';
import Search from '../tools/search';
import Colorize from '../tools/colorize';
import Loader from '../loader';
import Alert from '../alert';
const AppComponent = ({layout, searchEnvelope}) => (
    <div
        className="App"
        data-layout={layout}
    >
        <NavbarContainer/>
        <Alert/>
        <Loader/>
            {/*requestedFile &&
                <Redirect to="/load" component={Load}/>
            */}
        {searchEnvelope &&
            <Redirect to="/search" component={Load}/>
        }
        <Switch>
            <Route exact path='/style' component={Colorize}/>
            <Route exact path='/load' component={Load}/>
            <Route exact path='/search' component={Search}/>
            <Route component={Menu}/>
        </Switch>

        <MapContainer/>
    </div>
);

export default AppComponent;
