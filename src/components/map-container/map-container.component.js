import React from 'react';

const MapContainerComponent = () => (
    <div className={'map-container'}>
        <div id='map'/>
    </div>
);

export default MapContainerComponent;
