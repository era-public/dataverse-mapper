import React from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';

const AlertComponent = ({alert, hide}) => (
    <SweetAlert
        show={!!alert}
        title="That didn't work!"
        onConfirm={hide}
        confirmBtnCssClass='gt-button-accent'
    >{alert}</SweetAlert>
);

export default AlertComponent;
