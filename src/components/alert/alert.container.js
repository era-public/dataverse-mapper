import {hideAlert} from '../../actions/alert-actions';
import AlertComponent from './alert.component';
import {useDispatch, useSelector} from "react-redux";

const AlertContainer = () => {
    const alert = useSelector((state) => state.alert);
    const dispatch = useDispatch();
    const hide = () => {
        return dispatch(hideAlert());
    };
    return AlertComponent({alert, hide});
};

export default AlertContainer;
