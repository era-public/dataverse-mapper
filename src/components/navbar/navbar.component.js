import React from 'react';

const NavbarComponent = ({onToggleMenu, layout, showServices, onToggleGRO, showHelp, onToggleHelp, showKeyInput,
                             onToggleKeyInput, apiKey, onTokenRemove, onTokenChange, onToggleFAQ, showFAQ }) => (
    <ul id='navbar' className={'navbar'}>
        <div className='navbar-header' id='menu-header'>
            <div className={layout === 'default' ? 'menu-control-close' : 'menu-control-open'} onClick={() => {onToggleMenu()}} />
            <div className='navbar-icon'/>
        </div>
        <div className='navbar-right'>
            <button onClick={() => {onToggleKeyInput(); if (showHelp) onToggleHelp(); if (showFAQ) onToggleFAQ(); if (showServices) onToggleGRO();}}
                    className='navbar-right'>Add API key<i className='dropdown-icon'/>
            </button>
            {showKeyInput && <div className='apiKey-text smaller-text small-padding'>
                To access restricted data from {process.env.REACT_APP_DATAVERSE}, you can temporarily enter your API token.
                The token can be found and copied in the user menu of your &nbsp;
                <a target="_blank" rel="noreferrer" href={process.env.REACT_APP_DATAVERSE_URL} >{process.env.REACT_APP_DATAVERSE}</a>
                &nbsp; account. <br/>
                In order to automatically insert your token when opening {process.env.REACT_APP_TITLE},
                bookmark {process.env.REACT_APP_TITLE} as <i>{process.env.REACT_APP_DEFAULT_URL}?key=your-api-token</i>
                <div className='navbar-add-api-key'>
                    <input className='apikey-input' type='text'
                           defaultValue={apiKey}
                           onChange={e => onTokenChange(e.target.value)}
                           placeholder={'Insert your API token here'}/>
                    <button title='Remove token from current session' onClick={() =>{onTokenRemove(); onToggleKeyInput()}} className={'remove-apikey-button'}/>
                </div>
            </div> }
            <button onClick={() => {onToggleGRO(); if (showFAQ) onToggleFAQ(); if (showKeyInput) onToggleKeyInput(); if (showHelp) onToggleHelp(); }}
                    className='navbar-right'>GRO <i className='dropdown-icon'/>
            </button>
            {showServices && <div className="gro-dropdown">
                <a onClick={() => {onToggleGRO()}} target="_blank" href="https://data.goettingen-research-online.de/" rel="noreferrer">GRO.data</a>
                <a onClick={() => {onToggleGRO()}} target="_blank" href="https://publications.goettingen-research-online.de/" rel="noreferrer">GRO.publications</a>
                <a onClick={() => {onToggleGRO()}} target="_blank"  href="https://plan.goettingen-research-online.de/" rel="noreferrer">GRO.plan</a>
            </div> }
            <button onClick={() => {onToggleHelp(); if (showFAQ) onToggleFAQ(); if (showServices) onToggleGRO(); if (showKeyInput) onToggleKeyInput(); }}
                    className='navbar-right'>Help <i className='dropdown-icon'/>
            </button>
            {showHelp && <div className="gro-help">
                <a onClick={() => {onToggleFAQ()}}>Short FAQ</a>
                <a onClick={() => {onToggleHelp()}} target="_blank" href="https://gitlab.gwdg.de/era-public/dataverse-mapper/-/wikis/home" rel="noreferrer">Documentation</a>
            </div> }
            {showFAQ && <div className='faq-text smaller-text small-padding scrollable'>
                <b>What is GeoMapper?</b><br/>
                GRO.geomapper is a tool for visualizing geospatial files on a map, in particular data
                from <a target="_blank" rel="noreferrer" href={process.env.REACT_APP_DATAVERSE_URL} >{process.env.REACT_APP_DATAVERSE}</a>,
                the research repository of the <a rel="noreferrer" target="_blank"  href={'https://goettingen-campus.de/'}> Göttingen
                Campus</a>. The repository is based on the open-source software <a href={'https://dataverse.org/'}>Dataverse</a>.
                <br/>
                <b>Which files are supported?</b><br/>
                GeoTiff, GeoJSON, zipped Shapefiles, KML, and CSV with columns for latitude and longitude coordinates.<br/>
                <b>Where do I find more information?</b><br/>
                Please refer to our <a target="_blank" href="https://gitlab.gwdg.de/era-public/dataverse-mapper/-/wikis/home" rel="noreferrer">Documentation</a>.
            </div> }
            <a className='imprint' target="_blank" href="https://www.eresearch.uni-goettingen.de/imprint/" rel="noreferrer">Imprint</a>
        </div>
    </ul>
);

export default NavbarComponent;
