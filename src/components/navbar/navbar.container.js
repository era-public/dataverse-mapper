import NavbarComponent from './navbar.component';
import {useDispatch, useSelector} from "react-redux";
import {setDefaultLayout, setHideMenuLayout} from "../../actions/layout-actions";
import {useState} from "react";
import {addAPIKey, removeAPIKey} from "../../actions/api-actions";

const NavbarContainer = () => {
    const layout = useSelector((state) => state.layout);
    const apiKey = useSelector((state) => state.apiKey);
    const dispatch = useDispatch();
    const onTokenRemove = () => {
        dispatch(removeAPIKey());
    };
    const onTokenChange = (token) => {
        dispatch(addAPIKey(token));
    };
    const onToggleMenu = () => {
        if (layout !== 'hide-menu'){
            dispatch(setHideMenuLayout());
        } else dispatch(setDefaultLayout());
    };
    const onToggleGRO = () => {
        setShowServices(!showServices);
    };
    const onToggleHelp = () => {
        setShowHelp(!showHelp);
    };
    const onToggleFAQ = () => {
        setShowFAQ(!showFAQ);
    };
    const onToggleKeyInput = () => {
        setShowKeyInput(!showKeyInput);
    };
    const [showServices, setShowServices] = useState(false);
    const [showHelp, setShowHelp] = useState(false);
    const [showFAQ, setShowFAQ] = useState(false);
    const [showKeyInput, setShowKeyInput] = useState(false);
    return NavbarComponent({
        onToggleMenu, layout, showServices, onToggleGRO, showHelp, onToggleHelp, showKeyInput, onToggleKeyInput, apiKey,
        onTokenRemove, onTokenChange, onToggleFAQ, showFAQ});
};

export default NavbarContainer;
