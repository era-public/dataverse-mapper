import {RESULTS_CLEAR, RESULTS_ITEM_UPDATE, RESULTS_SET, RESULTS_UPDATE} from '../constants/actions';

export const setResults = results => ({
    results,
    type: RESULTS_SET,
});

export const updateResults = (obj) => ({
    obj,
    type: RESULTS_UPDATE,
});

export const updateResultItem = (obj) => ({
    obj,
    type: RESULTS_ITEM_UPDATE,
});

export const clearResults = () => ({
    type: RESULTS_CLEAR,
});
