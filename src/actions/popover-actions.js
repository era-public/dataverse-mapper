import {POPOVER_HIDE, POPOVER_SHOW} from '../constants/actions';

export const hidePopover = () => ({
    type: POPOVER_HIDE,
});

export const showPopover = () => ({
    type: POPOVER_SHOW,
});
