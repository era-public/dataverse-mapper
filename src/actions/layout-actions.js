import {LAYOUT_DEFAULT, LAYOUT_HIDE_MENU} from '../constants/actions';

export const setDefaultLayout = () => ({
    type: LAYOUT_DEFAULT,
});

export const setHideMenuLayout = () => ({
    type: LAYOUT_HIDE_MENU,
});
