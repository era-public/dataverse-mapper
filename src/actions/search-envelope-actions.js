import {SEARCH_ENVELOPE_ADDED, SEARCH_ENVELOPE_REMOVE} from "../constants/actions";

export const addSearchEnvelope = searchEnvelope => ({
    payload: searchEnvelope,
    type: SEARCH_ENVELOPE_ADDED,
});

export const removeSearchEnvelope = () => ({
    type: SEARCH_ENVELOPE_REMOVE,
});
