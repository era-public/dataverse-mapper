import {EXTERNAL_URL_ADDED, EXTERNAL_URL_REMOVED} from '../constants/actions';

export const setRequestedURL = url => ({
    payload: url,
    type: EXTERNAL_URL_ADDED,
});

export const removeRequestedURL = () => ({
    type: EXTERNAL_URL_REMOVED,
});
