import {API_KEY_ADDED, API_KEY_REMOVED} from '../constants/actions';

export const addAPIKey = apiKey => ({
    payload: apiKey,
    type: API_KEY_ADDED,
});

export const removeAPIKey = () => ({
    type: API_KEY_REMOVED,
});
