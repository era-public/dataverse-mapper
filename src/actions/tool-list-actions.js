import {TOOL_LIST_LOAD} from '../constants/actions';
import ToolListService from '../services/ToolListService';

export const loadTools = () => ({
    payload: ToolListService.getToolList(),
    type: TOOL_LIST_LOAD,
});
