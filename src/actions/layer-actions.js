import RasterService from '../services/RasterService';
import VectorService from '../services/VectorService';
import {
    MAP_LAYER_ADDED, MAP_LAYER_REMOVED,
    MAP_LAYER_SELECTED, MAP_LAYER_UNSELECTED,
} from '../constants/actions';
import Map from '../map/Map';
import {RASTER} from "../constants/variables";

export const addRaster = (input, fileName, fileID) => {
    return dispatch => {
        return RasterService.createRaster(input).then(raster => {
            raster.name = fileName;
            raster.id = fileID;
            raster.fileType = RASTER;
            try { Map.addRaster(raster) } catch (e) {return e}
            dispatch(addNewLayer(raster, fileID));
            return 'SUCCESS';
        }, error => {
            return error;
          });
    };
};

export const addNewLayer = (layer, layerID) => {
    return dispatch => {
       dispatch({type: MAP_LAYER_ADDED, payload: layer});
       dispatch({type: MAP_LAYER_SELECTED, payload: layerID});
        if (String(layerID).includes('_boundingBox_')) return; // do not zoom to bounding box
        try { Map.zoomToLayer(layer);
        } catch (e) {return e}
    };
};

export const removeLayer = id => {

    return dispatch => {
        dispatch({type: MAP_LAYER_REMOVED, payload: id});
        dispatch({type: MAP_LAYER_UNSELECTED, payload: id});
    };
};

export const updateSelectedLayer = layer => {
    return dispatch => {
        dispatch({type: MAP_LAYER_SELECTED, payload: layer.id});
    };
};

export const addGeoJSON = (geojson, fileName, fileID) => {
    return dispatch => {
        VectorService.addAttributes(geojson, fileName, fileID);
        try {
            const mapLayer = Map.addVectorLayer(geojson);
            dispatch(addNewLayer(mapLayer, mapLayer.id));
        } catch (e) {return e}
    };
};

export const addVectorData = (fileObj) => {
    return dispatch => {
        const file = fileObj.input;
        const fileName =  fileObj.fileName;
        const fileID = fileObj.fileID;
        const fileType = fileObj.fileType;
        const isRemoteURL = fileObj.isRemoteURL;
        return VectorService.createGeoJSON(file, fileName, fileID, fileType, isRemoteURL).then(geojson => {
            try {
                const mapLayer = Map.addVectorLayer(geojson);
                dispatch(addNewLayer(mapLayer, geojson.id));
            } catch (e) {return e}
            return 'SUCCESS';
        },  error => {
            return error;
        });
    };
};
