import {BOUNDING_BOX_ADD, BOUNDING_BOX_REMOVE} from "../constants/actions";

export const updateBoundingBox = geometry => ({
    type: BOUNDING_BOX_ADD, geometry,
});

export const removeBoundingBox = () => ({
    type: BOUNDING_BOX_REMOVE,
});
