import {MENU_FOCUS_OFF, MENU_FOCUS_ON} from '../constants/actions';

export const focusMenu = () => ({
    type: MENU_FOCUS_ON,
});

export const unfocusMenu = () => ({
    type: MENU_FOCUS_OFF,
});
