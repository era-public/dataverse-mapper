const UrlService = {

    params: null,
    get(param) {
        if (!this.params || Object.keys(this.params).length === 0) {
            this.params = new URLSearchParams(window.location.search);
        }
        return this.params.get(param);
    },

    removeAndReload(param) {
        if (!this.params || Object.keys(this.params).length === 0) {
            this.params = new URLSearchParams(window.location.search);
        }
        this.params.delete(param);
        const newURL = window.location.origin + window.location.pathname + '?' + this.params;
        window.history.replaceState({}, null, newURL);
    },

};

export default UrlService;
