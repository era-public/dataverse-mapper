import toolList from '../constants/tools.json';
import toolsLoaded from '../constants/tools-loaded.json';

const ToolListService = {

    getToolList() {
        if (toolsLoaded.tools) {
            const includeTheseTools = toolsLoaded.tools.map(tool => tool.toLowerCase().trim());
            return toolList.tools.filter(tool => includeTheseTools.includes(tool.param));
        } else {
            return toolList.tools;
        }
    },
};

export default ToolListService;
