import {open} from 'shapefile';
import shp from 'shpjs';
import fileToArrayBuffer from 'file-to-array-buffer';
import {GEOJSON, ZIPPED_SHAPEFILE, KML, ZIPPED_FILE, VECTOR, SHP_FILE, CSVFILE} from "../constants/variables";
import { kml } from "@tmcw/togeojson";
import randomColor from 'randomcolor';
import {readRemoteFile} from "react-papaparse";
import GeoJSON from "geojson";
import FileService from "./FileService";

const VectorService = {

    zipFileToGeojson(file) {
        return new Promise((resolve, reject) => {

            fileToArrayBuffer(file).then(data => {
                shp(data).then(geojson => {
                    resolve(geojson);
                }).catch(error => {
                   reject(error);
                });
            });

        });
    },

    readGeoJSONfromShp(reader, array) {

        return new Promise((resolve, reject) => {

            reader.read()
                .then(function log(result) {
                    if (result.done) {
                        const geojson = {'type': 'FeatureCollection', 'features': array};
                        resolve(geojson);
                        return;
                    }
                    array.push(result.value);
                    return reader.read().then(log);
                })
                .catch(error => {
                    reject(error);
                });

        });


    },

    addAttributes(geojson, fileName, fileID){
        geojson.name = fileName;
        geojson.id = fileID + '_' + Date.now();
        geojson.color = randomColor();
        geojson.fileType = VECTOR;
        if (geojson.features && geojson.features.length > 0) {
            geojson.geometry = geojson.features[0].geometry.type;
            if (geojson.features[0].properties && typeof geojson.features[0].properties  === 'object')
                geojson.attributes = Object.keys(geojson.features[0].properties);
        }
    },

    createGeoJSON(input, fileName, fileID, fileType, isRemoteURL) {

        if (isRemoteURL) {
            return this.createGeoJSONFromURL(input, fileName, fileID, fileType).then(value => {
                this.addAttributes(value, fileName, fileID);
                return value;
            });
        }
        return this.createGeoJSONFromFile(input, fileName, fileID, fileType).then(value =>{
            this.addAttributes(value, fileName, fileID);
            return value;
        });

    },

    createGeoJSONFromFile(file, fileName, fileID, fileType) {
        return new Promise((resolve, reject) => {

            switch (fileType) {
                case GEOJSON: {
                    const fileReader = new FileReader();
                    fileReader.onload = evt => {
                        const geojson = evt.target.result;
                        try {
                            const json = JSON.parse(geojson);
                            resolve(json);
                        } catch (e) {
                            reject('Error when reading GeoJSON! ', e);
                        }
                    };
                    fileReader.readAsText(file);
                    break;
                }
                case KML: {
                    const fileReader = new FileReader();
                    fileReader.onload = evt => {
                        const xml = evt.target.result;
                        try {
                            const kmlResult = kml(new DOMParser().parseFromString(xml, "text/xml"));
                            resolve(kmlResult);
                        } catch (e) {
                            reject('Error when reading KML! ', e);
                        }
                    };
                    fileReader.readAsText(file);
                    break;
                }
                case ZIPPED_FILE:
                case ZIPPED_SHAPEFILE: {
                    return this.zipFileToGeojson(file).then(value => resolve(value), reason => reject(reason));
                }
                case SHP_FILE: {
                    return fileToArrayBuffer(file)
                        .then(data => {
                            const chunks = [];
                            open(data)
                                .then(source => this.readGeoJSONfromShp(source, chunks, fileName, fileID))
                                .then(value => resolve(value), reason => reject(reason));
                        });
                }
                default:
                    reject('Unsupported file type!');
            }
        });

    },

    createGeoJSONFromURL(file, fileName, fileID, fileType) {
        return new Promise((resolve, reject) => {

            switch (fileType) {
                case GEOJSON: {
                    fetch(file)
                        .then(res => res.json())
                        .then(json => {
                            resolve(json);
                        }, error => {
                            reject(error);
                        });
                    break;
                }
                case KML: {
                    fetch(file)
                        .then(res => res.text())
                        .then(xml => {
                            const kmlResult = kml(new DOMParser().parseFromString(xml, "text/xml"));
                            resolve(kmlResult);
                        });
                    break;
                }
                case ZIPPED_FILE:
                case ZIPPED_SHAPEFILE: {
                    // download zip file and extract shapefile as geojson
                    fetch(file)
                        .then(res => res.body)
                        .then(stream => {
                            const chunks = [];
                            FileService.loadChunks(stream.getReader(), chunks)
                                .then(value => {
                                    const file = new File(value, fileID + '.zip', {type: 'application/zip'});
                                    this.zipFileToGeojson(file).then(value => resolve(value), reason => reject(reason));
                                }, reason => reject(reason));
                        }, error => {
                        reject(error);
                     });
                    break;
                }
                case SHP_FILE: {
                    const chunks = [];
                    return open(file)
                        .then(source => this.readGeoJSONfromShp(source, chunks, fileName, fileID))
                        .then(value => resolve(value), reason => reject(reason));
                }
                case CSVFILE: {
                    readRemoteFile(file, {
                        header: true, dynamicTyping: true, download: true, complete: (results) => {
                            const columns = results.meta.fields.map(function (value) {
                                return value.toUpperCase();
                            });
                            let data = results.data.map(item => {
                                return Object.fromEntries(
                                    Object.entries(item).map(([k, v]) => [k.toUpperCase(), v]),
                                );
                            });
                            const defaultLatitude = 'LATITUDE';
                            const defaultLongitude = 'LONGITUDE';
                            if (columns.includes(defaultLatitude) && columns.includes(defaultLongitude)) {
                                data = data.filter(function (item) {
                                    if (!item[defaultLatitude]) return false;
                                    else if (!item[defaultLongitude]) return false;
                                    return !isNaN(item[defaultLatitude]) && !isNaN(item[defaultLongitude]);
                                });
                                if (data.length === 0) {
                                    reject( 'No valid coordinates could be extracted!');
                                }
                                try {
                                    GeoJSON.parse(data, {Point: [defaultLatitude, defaultLongitude]},  result => {
                                        resolve(result);
                                    });
                                } catch(e) {reject(e)}
                            } else (reject('No columns LATITUDE and LONGITUDE found!'));
                        }, error: (e) => {
                            reject(e);
                        },
                    });
                    break;
                }
                default:
                    reject('Unsupported file type!');
            }
        });
    },

};

export default VectorService;
