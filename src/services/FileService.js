import {
    CSVFILE,
    GEOJSON,
    GEOTIFF,
    KML,
    SHP_FILE,
    UNSUPPORTED,
    ZIPPED_FILE,
    ZIPPED_SHAPEFILE,
} from "../constants/variables";
import {stopLoading} from "../actions/loading-actions";

const FileService = {

    getFileType (file)  {

        if (typeof file === 'object') {
            file = file.name.split('.')[1];
        } else if (file.includes('.')) {
            file = file.split('.').pop();
        }

        const csvConditions = ["csv", "separated-values", "tab"];
        const imageConditions = ["tif", "tiff", "geotiff"];
        const jsonConditions = ["geojson", "json", "geo+json"];
        const csvType = csvConditions.some(type => file.includes(type));
        const rasterType = imageConditions.some(type => file.includes(type));
        const jsonType = jsonConditions.some(type => file.includes(type));

        if (csvType) return CSVFILE;
        else if (rasterType) return GEOTIFF;
        else if (jsonType) return GEOJSON;
        else if (file.includes('zipped-shapefile')) return ZIPPED_SHAPEFILE;
        else if (file.includes('zip')) return ZIPPED_FILE;
        else if (file.includes('kml')) return KML;
        else if (file.includes('shp')) return SHP_FILE;

        return UNSUPPORTED;
    },

    processFileType (fileObj, addVectorData, addRaster, dispatch) {

        switch (fileObj.fileType) {
            case GEOTIFF:
                return dispatch(addRaster(fileObj.input, fileObj.fileName, fileObj.fileID));
            default:
                return dispatch(addVectorData(fileObj));
        }
    },

    getDataverseFileType (urlInput, fileID, dispatch){
        return new Promise((resolve, reject) => {
            fetch(urlInput, {method: 'HEAD'}).then(res => {
                if (res.status !== 200) dispatch(stopLoading());
                if (res.status === 403 || res.status === 401) {
                    if (urlInput.includes(process.env.REACT_APP_DATAVERSE_URL))
                        return reject("You don't have permission to access this data source! Please add a valid API token.");
                    return reject('You don\'t have permission to access this data source!');
                } else if (res.status === 404) {
                    return reject('Data source could not be found!');
                } else if (res.status !== 200) {
                    return reject('Error when loading URL!');
                }
                let metadata = res.headers.get('Content-Type');
                let fileType = urlInput.includes('.') ? urlInput.split('.').pop() : 'unknown';
                let fileName = fileID;
                if (metadata) {
                    fileType = FileService.getFileType(metadata);
                    fileName = metadata.match(/name="(.+)"/);
                    if (fileName && fileName.length > 0)  fileName = fileName[1];
                    else fileName = fileID + '.' + fileType;
                }
                const fileObj = {input: urlInput, fileType: fileType, fileID: fileID, fileName: fileName, isRemoteURL: true};
                resolve(fileObj);
            }).catch((e) => {
                return reject(`${process.env.REACT_APP_TITLE} was unable to load the file. ${e}`);
            });
        });
    },

    loadChunks(reader, array) {

        return new Promise((resolve, reject) => {

            reader.read()
                .then(function log(result) {

                    if (result.done) {
                        resolve(array);
                        return array;
                    }
                    array.push(result.value);
                    return reader.read().then(log);
                })
                .catch(error => {
                    reject(error);
                });

        });
    },

};

export default FileService;
