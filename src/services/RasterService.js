import geoblaze from 'geoblaze';
import GeoRasterLayer from 'georaster-layer-for-leaflet';
import FileService from "./FileService";

const RasterService = {

    getResolution() {
        const resolution = Number(new URLSearchParams(window.location.search).get('resolution'));
        if (resolution) return resolution;
        if (process.env.REACT_APP_RESOLUTION) {
          return Number(process.env.REACT_APP_RESOLUTION);
        }
        const width = document.documentElement.clientWidth;
        const userAgent = window.navigator.userAgent;
        const isAndroid = userAgent.includes('Android');
        const isChromebook = userAgent.includes('CrOS');
        if (width < 900 || isAndroid || isChromebook) {
            return 32;
        } else if (width < 1200) {
            return 48;
        } else {
            return 64;
        }
    },

    createMultiBandColors(georaster, band1Index, band2Index, band3Index, removeNoData) {
        return pixelValues => {
           let band1 = pixelValues[band1Index];
           let band2 = pixelValues[band2Index];
           let band3 = pixelValues[band3Index];
           if (!band1 || band1 === georaster.noDataValue) band1 = -1;
           if (!band2 || band2 === georaster.noDataValue) band2 = -1;
           if (!band3 || band3 === georaster.noDataValue) band3 = -1;
           if (band1 > 255) band1 = Math.round(band1 / 65536 * 255 * 2.55);
           if (band2 > 255) band2 = Math.round(band2 / 65536 * 255 * 2.55);
           if (band3 > 255) band3 = Math.round(band3 / 65536 * 255 * 2.55);
           let opacity = 1;
           if (removeNoData && band1 === -1 && band2 === -1 && band3 === -1) opacity = 0;
           return `rgba(${band1},${band2},${band3}, ${opacity})`;
       };
    },

    applyColorPalette(georaster, removeNoData){
        return pixelValues => {
            let opacity = 1;
            let singleBand = pixelValues[0];
            if (removeNoData && singleBand === georaster.noDataValue){
                opacity = 0;
            }
            let color = georaster.palette[singleBand];
            if (removeNoData && color[0] === 0 && color[1] === 0 && color[2] === 0) opacity = 0;
            return `rgba(${color[0]},${color[1]},${color[2]}, ${opacity})`;
        };
    },

    loadRasterFile(input) {
        return new Promise((resolve, reject) => {
            geoblaze.load(input)
                .then(georaster => {
                    const options = {
                        georaster: georaster,
                        debugLevel: 0,
                        opacity: 0.8,
                        resolution: this.getResolution(),
                        pane: 'georaster',
                    };
                    if (georaster.numberOfRasters > 2) {
                        options.pixelValuesToColorFn =
                            this.createMultiBandColors(georaster, 0, 1, 2, false);
                    } else if (georaster.palette) {
                        options.pixelValuesToColorFn =
                            this.applyColorPalette(georaster, false);
                    }
                    const raster = new GeoRasterLayer(options);
                    resolve(raster);
                }, error => {
                    reject(error);
                });
        });
    },

    createRaster(input) {
        let isURL = false;
        /* check for Github URL and switch to cors-enabled version */
        if (typeof input === 'string') {
            isURL = true;
            if (input.match(/^https:\/\/github\.com\/.*\/.*\/blob\/.*\/.*.tiff?$/i)) {
                input = input.replace('https://github.com/', 'https://raw.githubusercontent.com/').replace('/blob', '');
            } else if (input.match(/^https:\/\/github\.com\/.*\/.*\/blob\/.*\/.*.tiff?\?raw=true$/i)) {
                input = input.replace('https://github.com/', 'https://raw.githubusercontent.com/').replace('/blob', '').replace('?raw=true', '');
            } else if (input.match(/^https:\/\/github\.com\/.*\/.*\/raw\/.*\/.*.tiff?$/i)) {
                input = input.replace('https://github.com/', 'https://raw.githubusercontent.com/').replace('/raw/', '/');
            }
        }
        if (isURL){
            return new Promise((resolve, reject) => {
                fetch(input)
                .then(res => res.body)
                .then(stream => {
                    const chunks = [];
                    FileService.loadChunks(stream.getReader(), chunks)
                        .then(chunks => {
                            const file = new File(chunks, 'downloaded.geotiff', {type: 'image/tif'});
                            resolve(this.loadRasterFile(file));
                        }, error => reject(error));
                }, error => {
                    reject(error);
                });
            });
        } else return this.loadRasterFile(input);

    },
};


export default RasterService;
