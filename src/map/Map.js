import {GeoSearchControl, OpenStreetMapProvider} from 'leaflet-geosearch';
import {MapLayerControl} from './MapLayerControl';
import {MAP_LAYER_REMOVED, MAP_LAYER_SELECTED, MAP_LAYER_UNSELECTED} from '../constants/actions';
import L from 'leaflet';
import {MapLegend} from "./MapLegend";
import {VECTOR} from "../constants/variables";
import 'leaflet-basemaps';
import 'leaflet-basemaps/L.Control.Basemaps.css';
import 'leaflet.fullscreen';
import 'leaflet-editable';
import topoThumbnail from '../assets/images/topomap.png';
import 'leaflet-draw';
import 'leaflet-draw/dist/leaflet.draw.css';
import {removeBoundingBox} from "../actions/bb-actions";
import {addSearchEnvelope, removeSearchEnvelope} from "../actions/search-envelope-actions";
import {MapWMSOptions} from "./MapWMSOptions";

let store;
let map, layerControl, legend, wmsOptions, drawnItems, drawControl, editBar, mapSearching;
const Map = {

    initialize() {
        store = window.store;
        mapSearching = false;
        const osmMap =
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                label: 'OSM',
            });
        const colorMap =  L.tileLayer('https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg', {
            attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. ' +
                'Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://creativecommons.org/licenses/by-sa/3.0">CC BY SA</a>.',
            label: 'WaterColor',
        });
        const terrainMap =
            L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.png', {
                attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, ' +
                    '<a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; ' +
                    '<a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                label: 'Terrain',
            });
        const tonerMap =
            L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png', {
                attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, ' +
                    '<a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; ' +
                    '<a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                label: 'Toner',
            });
        const topoMap =
            L.tileLayer('https://c.tile.opentopomap.org/{z}/{x}/{y}.png', {
                attribution: '&#169; OpenStreetMap-Mitwirkende, SRTM | Kartendarstellung: &#169; OpenTopoMap (CC-BY-SA)',
                label: 'Topo',
                iconURL: topoThumbnail,
            });

        map = L.map('map', {
            zoomControl: false,
            zoomSnap: 0.5,
            minZoom: 2,
            tap: false, //enabling can cause errors in mobile view
            editable: true,
            layers: [terrainMap],
        }).setView([35, -20], 2.5);

        map.createPane('georaster');
        map.getPane('georaster').style.zIndex = 401;
        map.getPane('georaster').style.pointerEvents = 'none';
        new GeoSearchControl({
            provider: new OpenStreetMapProvider(),
            position: 'topleft',
            searchLabel: 'Search by address',
        }).addTo(map);

        L.control.fullscreen({position: 'topleft', forceSeparateButton: true}).addTo(map);
        L.control.zoom({position: 'topleft'}).addTo(map);
        L.control.basemaps({
            basemaps: [terrainMap, topoMap, colorMap, osmMap, tonerMap],
            position: 'bottomright',
        }).addTo(map);
        layerControl = new MapLayerControl({'Topo': topoMap}, [], {hideSingleBase: true, collapsed: false}).addTo(map);
        legend = new MapLegend({position: 'topright'}).addTo(map);
        wmsOptions = new MapWMSOptions({position: 'topright'}).addTo(map); // add WMS layer control window
        const WMSOptionsControl = L.Control.extend({ // add WMS layer control button
            options: {position: 'topleft'},
            onAdd: function () {
                const controlDiv = L.DomUtil.create('div', 'leaflet-control');
                L.DomEvent
                    .addListener(controlDiv, 'click', L.DomEvent.stopPropagation)
                    .addListener(controlDiv, 'click', L.DomEvent.preventDefault)
                    .addListener(controlDiv, 'click',  () => {
                        if (wmsOptions.isHidden()) wmsOptions.show();
                        else wmsOptions.hide();
                    });
                const controlUI = L.DomUtil.create('div', 'leaflet-wmslayer', controlDiv);
                controlUI.title = 'Add external WMS layer';
                return controlDiv;
            },
        });
        map.addControl(new WMSOptionsControl());

        drawnItems = new L.FeatureGroup();
        map.addLayer(drawnItems);
        drawControl = new L.Control.Draw({
           draw: {
               circle: false,
               polygon: false,
               marker: false,
               circlemarker: false,
               rectangle: true,
           }, edit: {
               featureGroup: drawnItems,
            },
        });
        editBar = new L.EditToolbar.Edit(map,{
            featureGroup: drawnItems,
        });
        L.drawLocal.draw.toolbar.buttons.rectangle = 'Draw a bounding box to retrieve coordinates for your dataset metadata';
        L.drawLocal.draw.toolbar.buttons.polyline = 'Measure approximate distance by drawing a polyline';
        L.drawLocal.edit.toolbar.actions.clearAll.title = 'Clear all drawn items';
        L.drawLocal.edit.toolbar.buttons.remove = 'Delete drawn items';
        L.drawLocal.edit.toolbar.buttons.removeDisabled = 'No drawn items to delete';
        L.drawLocal.edit.toolbar.buttons.editDisabled = 'No drawn items to edit';
        L.drawLocal.edit.toolbar.buttons.edit = 'Edit drawn items';
        L.drawLocal.draw.handlers.rectangle.tooltip.start = 'Click and drag to draw bounding box';
        map.addControl(drawControl);
        map.on('draw:deleted', () => {
            let count = 0;
            drawnItems.getLayers().forEach(layer => {
               if (layer.type && layer.type === 'rectangle')
                   count++;
            });
            if (count === 0) store.dispatch(removeBoundingBox());
        });
        L.DomEvent
            .addListener(document.getElementsByClassName('leaflet-draw-draw-rectangle')[0], 'click',
                () => { if (mapSearching) mapSearching = false; });
        L.DomEvent
            .addListener(document.getElementsByClassName('leaflet-draw-draw-polyline')[0], 'click',
                () => {
                if (mapSearching) mapSearching = false;
                if (this.rectangleDrawer) this.rectangleDrawer.disable();
            });
        map.on(L.Draw.Event.CREATED, (e) => {
            const type = e.layerType, drawnItem = e.layer;
            drawnItem.type = type;
            if (mapSearching){
                if (type === 'rectangle') {
                    store.dispatch(removeSearchEnvelope());
                    const bounds = this.validateBounds(drawnItem.getBounds());
                    const envelope = `ENVELOPE(${bounds['west']},${bounds['east']},${bounds['north']},${bounds['south']})`;
                    this.stopDrawRectangle(false);
                    store.dispatch(addSearchEnvelope(envelope));
                    mapSearching = false;
                }
                return;
            }
            drawnItems.addLayer(drawnItem);
            if (type === 'rectangle') {
                this.saveBoundingBox(true); //create popup
            }
        });
        const MapSearchControl = L.Control.extend({
            options: {position: 'topleft'},
            onAdd: function () {
                const controlDiv = L.DomUtil.create('div', 'leaflet-control');
                L.DomEvent
                    .addListener(controlDiv, 'click', L.DomEvent.stopPropagation)
                    .addListener(controlDiv, 'click', L.DomEvent.preventDefault)
                    .addListener(controlDiv, 'click',  () => {
                        mapSearching = true;
                        Map.startDrawRectangle();
                    });
                const controlUI = L.DomUtil.create('div', 'leaflet-mapsearch', controlDiv);
                controlUI.title = 'Search '+process.env.REACT_APP_DATAVERSE+' by bounding box';
                return controlDiv;
            },
        });
        new MapSearchControl().addTo(map);
    },

    saveBoundingBox(openPopup){
        this.saveDrawings(); //edits
        let rectangles = [];
        drawnItems.getLayers().forEach(layer => {
            if (!layer.type || layer.type !== 'rectangle')
                return;
            const bounds = this.validateBounds(layer.getBounds());
            const south = bounds['south'];
            const north = bounds['north'];
            const west = bounds['west'];
            const east = bounds['east'];
            const bbText = "<h3><b>Geographic Bounding Box</b><br/></h3>" +
                "In <a target='_blank' href='"+process.env.REACT_APP_DATAVERSE_URL+"' rel='noreferrer'>"+process.env.REACT_APP_DATAVERSE+"</a>, "+
                "a bounding box defines the minimum box which includes the largest geographic extent of a Dataset's geographic coverage.</br>"+
                "</br>To define a new bounding box, first click on your Dataset's <i>Edit Bounding Box</i> button in the search result list, " +
                "then draw or edit a bounding box and click on <i>Submit</i> in the side bar. " +
                "This will overwrite the bounding box(es) of your Dataset's draft version (if existent) or otherwise create a draft " +
                " which you can then publish as new minor Dataset version.</br>"+
                "<br>Note: Values exceeding the allowed ranges (-180 to 180 for longitude, -90 to 90 for latitude) will be converted to " +
                "their default (maximum) values automatically!<br/><table><br>" +
                "<tr><th><b>North Latitude:</b></th><td>"+north+"</td></tr><tr>"+
                "<tr><th><b>South Latitude:</b></th><td>"+south+"</td></tr><tr>"+
                "<tr><th><b>East Longitude:</b></th><td>"+east+"</td></tr><tr>"+
                "<tr><th><b>West Longitude:</b></th><td>"+west+"</td></tr>"+
                "</table></br>";
            layer.bindPopup(bbText);
            if (openPopup) layer.openPopup();
            let currentBBox = {
                "westLongitude": {
                    "typeName": "westLongitude",
                    "multiple": false,
                    "typeClass": "primitive",
                    "value": String(west),
                },
                "eastLongitude": {
                    "typeName": "eastLongitude",
                    "multiple": false,
                    "typeClass": "primitive",
                    "value": String(east),
                },
                "northLongitude": {
                    "typeName": "northLongitude",
                    "multiple": false,
                    "typeClass": "primitive",
                    "value": String(north),
                },
                "southLongitude": {
                    "typeName": "southLongitude",
                    "multiple": false,
                    "typeClass": "primitive",
                    "value": String(south),
                },
            };
            rectangles.push(currentBBox);
        });
        return rectangles;
    },
    validateBounds(bounds) {
        let west = bounds.getWest();
        let east = bounds.getEast();
        let north = bounds.getNorth();
        let south = bounds.getSouth();
        // envelope allowed in range: ENVELOPE(-180,180,90,-90)
        west = (west >= -180 && west <= 180) ? west : -180.0;
        east = (east >= -180 && east <= 180) ? east : 180.0;
        north = (north >= -90 && north <= 90) ? north : -90.0;
        south = (south >= -90 && south <= 90) ? south : 90.0;
        return {"west": west, "east": east, "north": north, "south": south};
    },
    startEditRectangle(rectangles) {
        drawnItems.clearLayers(); //clear previous drawings
        rectangles.forEach(rectangle => {
            let layer = L.rectangle(rectangle).addTo(drawnItems);
            layer.type = 'rectangle';
            layer.enableEdit();
        });
    },

    saveDrawings(){
        editBar.enable();
        editBar.save();
        editBar.disable();
    },

    startDrawRectangle() {
        this.rectangleDrawer = new L.Draw.Rectangle(map, {});
        this.rectangleDrawer.enable();
    },

    stopDrawRectangle(clearLayers) {
        if (clearLayers) drawnItems.clearLayers();
        if (this.rectangleDrawer) {
            this.rectangleDrawer.disable();
        }
    },

    showLegend(layerID, attribute, colors, displayValues, otherValues) {
        if (attribute === 'noAttr') return;
        const layer = this.getLayerById(layerID);
        legend.show(layer.name, attribute, colors, displayValues, otherValues);
    },

    hideLegend() {
        if (legend) legend.hide();
    },

    getLayerById(id) {
        const storedLayers = store.getState().layers;
        const storedLayer = storedLayers.filter(layer => String(layer.id) === String(id));
        return storedLayer[0];
    },

    getBoundingBoxLayers() {
        const storedLayers = store.getState().layers;
        return storedLayers.filter(layer => String(layer.id).includes('_boundingBox'));
    },

    removeBoundingBoxLayers() {
      const layersToRemove = this.getBoundingBoxLayers();
      layersToRemove.forEach(layer => {
          this.removeLayer(layer);
      });
    },

    getAttributesForLayer(id) {
        const layer = this.getLayerById(id);
        if (!layer) return;

        const attributeList = [];
        for (let i = 0; i < layer.attributes.length; i++) {
            const attr = layer.attributes[i];
            attributeList.push({'key': i, 'value': attr, 'label': attr});
        }
        if (attributeList.length < 1) {
            attributeList.push({'key': 0, 'value': 'noAttr', 'label': 'No attributes available!'});
        }
        return attributeList;

    },

    getAttributeValues(id, attribute, onlyUnique) {
        const geojsonLayer = this.getLayerById(id);
        const values = [];
        geojsonLayer.eachLayer(layer => {
            if (typeof (layer.feature) === 'undefined') {
                return;
            }
            const value = layer.feature.properties[attribute];
            if (!onlyUnique) values.push(value);
            else if (onlyUnique && !values.includes(value)) {
                values.push(value);
            }
        });
        return values.sort();
    },

    updateRasterLayout(id) {
        const layerToUpdate = this.getLayerById(id);
        if (layerToUpdate) {
            layerToUpdate.redraw();
        }
    },

    updateVectorStyle(layerID, style) {
        const layer = this.getLayerById(layerID);
        layer.setStyle(style);
    },

    updateVectorStyleByValue(layerID, attribute, colorMap) {
        const layer = this.getLayerById(layerID);
        const style = feature => {
            const value = feature.properties[attribute];
            let color = colorMap[value];
            if (!color) color = '#FFF';
            return this.getVectorStyle(color);
        };
        layer.setStyle(style);
    },

    getColorFromClassBreaks(value, classBreaks, colors){

        for (let index = 0; index < classBreaks.length; index++){
            const threshold = classBreaks[index];
            if (value === threshold) return colors[index];
            if (index === 0 && value <= threshold) return colors[index];
            if (index === classBreaks.length-1) return colors[index];
            else if (classBreaks[index+1] > value)  {
                return colors[index];
            }
        }
    },

    updateVectorStyleByClass(layerID, attribute, colors, classBreaks) {
        const layer = this.getLayerById(layerID);
        let color;
        const style = feature => {
            const value = parseFloat(feature.properties[attribute]);
            color = this.getColorFromClassBreaks(value, classBreaks, colors);
            if (!color) color = '#FFF';
            return this.getVectorStyle(color);
        };
        layer.setStyle(style);
    },

    addRaster(layer) {
        this.hideLegend();
        layer.addTo(map);
        const layerBounds = layer.getBounds();
        map.flyToBounds(layerBounds, { maxZoom: 12 }).on('zoomend', function () {
            layer.bringToFront();
        });
        layerControl.addOverlay(layer, layer.name);
    },

    getVectorStyle(color) {
        return {
            radius: 7,
            color: '#000',
            weight: 2,
            fillOpacity: 1,
            fillColor: color,
        };
    },

    getTransparentPolygonStyle(color){
        return {
            color: '#000',
            weight: 8,
            fillColor: color,
            fillOpacity: 0.5,
        };
    },

    zoomToLayer(layer) {

        const bounds = layer.getBounds();
        const southWest = bounds.getSouthWest();
        const northEast = bounds.getNorthEast();
        const coords = [southWest.lat, southWest.lng, northEast.lat, northEast.lng];
        let isValid = true;
        coords.forEach(item => {
            if (typeof (item) === 'undefined' || String(item).includes('e')) isValid = false;
        });
        if (isValid) map.fitBounds(bounds, {maxZoom: 13});
    },

    bindPopup(feature, layer) {
        if (feature.properties && Object.keys(feature.properties).length > 0) {
            let content = '';
            for (const [key, v] of Object.entries(feature.properties)) {
                let value = v;
                if (!key ||  String(key).trim() === '') continue;
                if (!value || String(value).trim() === '') value = '-';
                else if (/^http|^https/.test(v)) value = "<a target=\"_blank\" href=\""+ v +"\">Website</a>";
                content += '<b>' + key + '</b>: ' + value + '<br>';
            }
            layer.bindPopup('<p> ' + content + '</p>');
        } else {
            layer.bindPopup('<p>No attributes available</p>');
        }
    },

    addWMSLayer(layerURL, layerName){
        let loadMessage = document.getElementById('wmsLoadMessage');
        if (!layerURL.includes('http') || layerURL.trim() === '' || layerName.trim() === ''){
            loadMessage.innerHTML = "Please provide valid input!";
            return;
        }
        const wmsLayer = L.tileLayer.wms(layerURL, {
            layers: layerName,
        });
        wmsLayer.externalWMS = true;
        wmsLayer.addTo(map);
        wmsLayer.on("load",function() {
            if (loadMessage && loadMessage.innerHTML.includes('Error')) loadMessage.innerHTML = "Done loading, with errors.";
            else loadMessage.innerHTML = "Done loading.";
        });
        wmsLayer.on("tileerror",function() {
            loadMessage.innerHTML = "Error loading tiles!";
        });
    },

    clearWMSLayers(){
        map.eachLayer(function(layer){
            if (layer.externalWMS){
                map.removeLayer(layer);
            }
        });
    },

    addVectorLayer(geojson) {
        let color = '#F5A623';
        if (geojson.color) color = geojson.color;
        let defaultStyle = this.getVectorStyle(color);
        if (geojson.geometry === 'Polygon' && geojson.attributes.includes('Dataverse')){
            defaultStyle = this.getTransparentPolygonStyle(color);
        }
        const bindPopup = this.bindPopup;
        const geojsonLayer = L.geoJson(geojson, {
            pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, defaultStyle);
            },
            onEachFeature: function (feature, layer) {
                bindPopup(feature, layer);
            },
            style: defaultStyle,
        });

        geojsonLayer.id = geojson.id;
        geojsonLayer.name = geojson.name;
        geojsonLayer.fileType = VECTOR;
        geojsonLayer.color = color;
        geojsonLayer.geometry = geojson.geometry;
        geojsonLayer.attributes = geojson.attributes;
        geojsonLayer.addTo(map);
        layerControl.addOverlay(geojsonLayer, geojsonLayer.name);
        return geojsonLayer;

    },

    removeAll () {
        const storedLayers = store.getState().layers;
        storedLayers.forEach(layer => {
            this.removeLayer(layer);
        });
    },

    removeLayer(layer) {
        map.removeLayer(layer);
        layerControl.removeLayer(layer);
        legend.hide();

        store.dispatch({type: MAP_LAYER_REMOVED, payload: layer.id});
        store.dispatch({type: MAP_LAYER_UNSELECTED, payload: layer.id});
        const storedLayers = store.getState().layers;
        if (storedLayers.length > 0){
            const nextLayer = storedLayers[storedLayers.length - 1];
            store.dispatch({type: MAP_LAYER_SELECTED, payload: nextLayer.id});
        }
        if (legend.displayedLayer === layer.id) { //legend only shows one layer
            this.hideLegend();
        }
    },

    bringLayerBack(layer){
        layer.bringToBack();
    },

};

export default Map;
