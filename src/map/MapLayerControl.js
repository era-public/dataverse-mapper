import Map from './Map';
import L from 'leaflet';

const createButton = (className) => {
    const button = document.createElement('button');
    button.className = className;
    return button;
};

export const MapLayerControl = L.Control.Layers.extend({
    onAdd: function () {
        this._initLayout();
        this._update();
        return this._container;
    },
    addOverlay: function (layer, name) {
        this._addLayer(layer, name, true);
        return (this._map) ? this._update() : this;
    },
    _getOverlays: function() {
        let overlays = [];
        for (let i = this._layers.length-1; i >= 0; i--) {
            if (this._layers[i].overlay) {
                overlays.push(this._layers[i]);
            }
        }
        return overlays;
    },
    _createHeader: function () {
        const overview = document.createElement('div');
        const title = document.createElement('span');
        const collapseButton = createButton('collapse-button right');
        collapseButton.title = "Collapse/expand";
        const clearButton = createButton('clear-layer-button right');
        clearButton.title = "Remove all layers";
        overview.id = 'layer-control-overview';
        overview.className = 'clear-float small-padding';
        title.className = 'small-padding small-text';
        title.innerHTML = "<b>Layers</b>";
        overview.append(title);
        overview.append(collapseButton);
        overview.append(clearButton);
        L.DomEvent.on(collapseButton, 'click', e => {
            L.DomEvent.stop(e);
            collapseButton.className =
                collapseButton.className === 'collapse-button right' ? 'collapse-button-open right' : 'collapse-button right';
            this._overlaysList.className =  this._overlaysList.className === 'invisible' ? '' : 'invisible';
        }, this);
        L.DomEvent.on(clearButton, 'click', e => {
            L.DomEvent.stop(e);
            Map.removeAll();
        }, this);
        this._container.prepend(overview);
    },
    _update: function () {

        if (!this._container) {
            return this;
        }

        L.DomUtil.empty(this._baseLayersList);
        L.DomUtil.empty(this._overlaysList);

        this._layerControlInputs = [];
        let baseLayersPresent, overlaysPresent, i, obj, baseLayersCount = 0;
        let sortedItems = [];
        let layerAdded = false;
        for (i = this._layers.length-1; i >= 0; i--) { //newest layer on top
            obj = this._layers[i];
            overlaysPresent = overlaysPresent || obj.overlay;
            if (typeof obj.position === 'undefined') {
                obj.position = 0;
                layerAdded = true;
            }
            if (obj.overlay) sortedItems.push(obj); //add to sort
            baseLayersPresent = baseLayersPresent || !obj.overlay;
            baseLayersCount += !obj.overlay ? 1 : 0;
        }
        if (sortedItems.length > 0){
            if (layerAdded){
                for (let i = 1; i < sortedItems.length; i++) { //increment positions after insert
                    sortedItems[i].position += 1;
                }
            }
            sortedItems.sort((a,b) => (a.position > b.position) ? 1 : ((b.position > a.position) ? -1 : 0));
            sortedItems.forEach(item => {
                this._addItem(item.position, item);
            });
        }

        // Hide control if no overlays are present because basemaps are handled by separate control
        this._container.style.display = overlaysPresent ? '' : 'none';
        // Hide base layers section if there's only one layer.
        if (this.options.hideSingleBase) {
            baseLayersPresent = baseLayersPresent && baseLayersCount > 1;
            this._baseLayersList.style.display = baseLayersPresent ? '' : 'none';
        }
        this._separator.style.display = overlaysPresent && baseLayersPresent ? '' : 'none';
        if (overlaysPresent && !document.getElementById('layer-control-overview')){
           this._createHeader();
        }

        return this;
    },
    _sortByPosition(overlays){
        overlays.sort((a,b) => (a.position > b.position) ? 1 : ((b.position > a.position) ? -1 : 0));
        return overlays;
    },
    _movePosition: function (layer, oldPosition, newPosition){

        if (!layer) return;
        let overlays = this._getOverlays();
        overlays = this._sortByPosition(overlays);
        if (newPosition === -1 || newPosition === overlays.length) return;
        L.DomUtil.empty(this._overlaysList);
        overlays[oldPosition].position = newPosition;
        overlays[newPosition].position = oldPosition;

        return this._update();
    },
    _addItem: function (position, obj) {

        let label = document.createElement('label'),
            checked = this._map.hasLayer(obj.layer),
            input;

        let removeButton, zoomToButton, moveUpButton, moveDownButton;

        if (obj.overlay) {
            obj.position = position;
            obj.layer.position = position;
            if (obj.layer.fileType && obj.layer.fileType === 'raster') {
                if (position === 0) this._map.getPane('georaster').style.zIndex = 401;
            } else {
                if (position === 0) this._map.getPane('georaster').style.zIndex = 399;
                obj.layer.bringToBack();
            }

            input = document.createElement('input');
            input.type = 'checkbox';
            input.className = 'leaflet-control-layers-selector';
            input.defaultChecked = checked;

            removeButton = createButton('remove-button right');
            removeButton.title = "Remove layer";
            zoomToButton = createButton('zoom-layer-button right');
            zoomToButton.title = "Zoom to layer";
            moveUpButton = createButton('move-up-button right');
            moveUpButton.title = "Move layer up";
            moveDownButton = createButton('move-down-button right');
            moveDownButton.title = "Move layer down";
            
            L.DomEvent.on(removeButton, 'click', e => {
                L.DomEvent.stop(e);
                Map.removeLayer(obj.layer);
            }, this);
            L.DomEvent.on(zoomToButton, 'click', e => {
                L.DomEvent.stop(e);
                Map.zoomToLayer(obj.layer);
            }, this);
            L.DomEvent.on(moveUpButton, 'click', e => {
                L.DomEvent.stop(e);
                this._movePosition(obj.layer, position, position-1);
            }, this);
            L.DomEvent.on(moveDownButton, 'click', e => {
                L.DomEvent.stop(e);
                this._movePosition(obj.layer, position, position+1);
            }, this);

        } else {
            input = this._createRadioElement('leaflet-base-layers_' + L.Util.stamp(this), checked);
        }

        input.position = position;
        this._layerControlInputs.push(input);
        input.layerId = L.Util.stamp(obj.layer);

        if (!obj.overlay) L.DomEvent.on(input, 'click', this._onInputClick, this);
        else L.DomEvent.on(input, 'click', () => {
            this._onOverlayChecked(input);
        }, this);

        const name = document.createElement('span');
        name.innerHTML = ' ' + obj.name;
        name.title = obj.name; // add tooltip for layer name
        const holder = document.createElement('div');
        holder.className = 'layer-control-item';

        label.appendChild(holder);
        holder.appendChild(input);
        holder.appendChild(name);
        if (obj.overlay) {
            holder.appendChild(zoomToButton);
            holder.appendChild(removeButton);
            holder.appendChild(moveDownButton);
            holder.appendChild(moveUpButton);
        }

        const container = obj.overlay ? this._overlaysList : this._baseLayersList;
        container.appendChild(label);

        this._checkDisabledLayers();
        return label;
    },
    _onOverlayChecked: function (input) {

        const layer = this._getLayer(input.layerId).layer;
        if (!input.checked){
            if (this._map.hasLayer(layer)) {
                this._map.removeLayer(layer);
            }
        } else this._map.addLayer(layer);

    },
});
