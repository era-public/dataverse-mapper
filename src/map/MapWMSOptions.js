import L from 'leaflet';
import Map from './Map';

export const MapWMSOptions = L.Control.extend({
   onAdd: function () {
       this._init();
       return this._container;
    },
    _init(){
        this._container = L.DomUtil.create('div', 'info leaflet-control-layers wms-options-panel invisible');
        this._container.id = 'wmsOptionsPanel';
        const closeButton = L.DomUtil.create('button', 'remove-button right');
        L.DomEvent.on(closeButton, 'click', function (e) {
            L.DomEvent.stop(e);
            this.hide();
        }, this);
        const dropDownButton = L.DomUtil.create('button', 'collapse-button right');
        const dropDownHeader = L.DomUtil.create('div', 'small-padding');
        const toolContent = L.DomUtil.create('div', 'scrollable');
        const collapsable = L.DomUtil.create('div');
        const title = L.DomUtil.create('span', 'small-padding small-text');
        const description = L.DomUtil.create('div', 'small-padding smallest-text wms-description');
        const clearButton = L.DomUtil.create('button', 'clear-layer-button right');
        clearButton.title = "Remove all WMS layers";
        title.innerHTML = "<b>Add WMS Layer</b>";
        description.innerHTML = "You can add an external Web Map Service layer by providing the base URL to the service and the name of the requested layer.<br>";
        toolContent.append(description);
        const wmsInput = L.DomUtil.create('input', "wmsInput smallMargin", toolContent);
        const wmsNameInput = L.DomUtil.create('input', "wmsInput smallMargin", toolContent);
        wmsInput.placeholder = "e.g. https://example.com/geoserver/wms";
        wmsInput.id = "wmsInput";
        wmsNameInput.id = "wmsNameInput";
        wmsNameInput.placeholder = "Layer name within the service";
        const loadWMSButton = L.DomUtil.create('button', 'load-wms-button marginLeft smallMargin');
        loadWMSButton.innerHTML = 'Load Layer';
        const loadMessage = L.DomUtil.create('span', 'small-padding small-text');
        loadMessage.id = 'wmsLoadMessage';
        toolContent.append(loadWMSButton);
        toolContent.append(loadMessage);
        L.DomEvent.on(loadWMSButton, 'click', function (e) {
            L.DomEvent.stop(e);
            loadMessage.innerHTML = "Loading...";
            const layerURL = L.DomUtil.get("wmsInput").value;
            const layerName = L.DomUtil.get("wmsNameInput").value;
            Map.addWMSLayer(layerURL, layerName);
        }, this);

        L.DomEvent.on(dropDownButton, 'click', function (e) {
            L.DomEvent.stop(e);
            dropDownButton.className =
                dropDownButton.className === 'collapse-button right' ? 'collapse-button-open right' : 'collapse-button right';
            collapsable.className = collapsable.className === 'invisible' ? '' : 'invisible';
        }, this);

        L.DomEvent.on(clearButton, 'click', function (e) {
            L.DomEvent.stop(e);
            Map.clearWMSLayers();
            loadMessage.innerHTML = "External layers removed!";
        }, this);
        dropDownHeader.appendChild(title);
        dropDownHeader.appendChild(dropDownButton);
        dropDownHeader.appendChild(closeButton);
        dropDownHeader.append(clearButton);
        collapsable.append(toolContent);
        this._container.appendChild(dropDownHeader);
        this._container.appendChild(collapsable);

        return this._container;
    },
    isHidden: function() {
        if (!this._container) return true;
        return this._container.classList.contains('invisible');
    },
    show: function () {
        if (!this._container) this._container = this._init();
        this._container.classList.remove("invisible");
        document.getElementById('wmsLoadMessage').innerHTML = "";
    },
    hide: function(){
       this._container.classList.add("invisible");
    },
});
