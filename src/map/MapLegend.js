import L from 'leaflet';

export const MapLegend = L.Control.extend({
    onAdd: function () {
        this._init();
        return this._container;
    },
    _init(){
        this._container = L.DomUtil.create('div', 'info legend invisible');
        const dropDownHeader = L.DomUtil.create('div', 'inline-flex');
        const legendContent = L.DomUtil.create('div', 'scrollable');
        const closeButton = L.DomUtil.create('button', 'remove-button fixed-button-size flex-right');
        L.DomEvent.on(closeButton, 'click', function (e) {
            L.DomEvent.stop(e);
            this.hide();
        }, this);
        const dropDownButton = L.DomUtil.create('button', 'collapse-button fixed-button-size right');
        const layerTitle = L.DomUtil.create('span' );
        const collapsable = L.DomUtil.create('div');
        const subTitle = L.DomUtil.create('span');
        L.DomEvent.on(dropDownButton, 'click', function (e) {
            L.DomEvent.stop(e);
            dropDownButton.className =
                dropDownButton.className === 'collapse-button fixed-button-size right' ? 'collapse-button-open fixed-button-size right' :
                    'collapse-button fixed-button-size right';
            collapsable.className = collapsable.className === 'invisible' ? '' : 'invisible';
        }, this);
        subTitle.id = 'legend-subheader';
        legendContent.id = 'inner-legend';
        layerTitle.id = 'legend-header';
        layerTitle.className = 'legend-header';
        dropDownHeader.appendChild(layerTitle);
        dropDownHeader.appendChild(closeButton);
        dropDownHeader.appendChild(dropDownButton);
        collapsable.appendChild(subTitle);
        collapsable.append(legendContent);
        this._container.appendChild(dropDownHeader);
        this._container.appendChild(collapsable);
        return this._container;
    },
    show: function (layerName, attribute, colors, values, otherValues) {
        if (!this._container) this._container = this._init();

        document.getElementById('legend-header').innerHTML  =
            '<b>'+ layerName + '</b>';
        document.getElementById('legend-subheader').innerHTML = '<span class = "legend-subheader">'+attribute+'</span><br>';
        const legendContent = document.getElementById('inner-legend');
        L.DomUtil.empty(legendContent);
        legendContent.innerHTML = "Loading attributes ...";
        let content = "";
        values.forEach((value, index) => {
            let color = colors[index];
            if (value && !isNaN(value)) {
                if (value % 1 === 0) value = Number(value);
                else value = Number(value).toFixed(2).toLocaleString('en-US');
            }
            content += '<i class="legend-entry border small-padding" style="background:' + color + '"></i><span class="small-padding">' + value + '</span><br>';
            content += '<div style=\'margin-top: 2px; clear:left\'></div>';
        });
        if (otherValues) {
            content += '<i class="legend-entry border small-padding"  style="background: #FFF" ></i><span class="small-padding">NoValue</span><br>';
        }
        legendContent.innerHTML = content;
        this._container.classList.remove("invisible");
        L.DomEvent.disableClickPropagation(legendContent);
        L.DomEvent.disableScrollPropagation(legendContent);
    },
    hide: function(){
        this._container.classList.add("invisible");
    },
});
